<?php
    
    class BidsModel extends DB\SQL\Mapper
    {
        function __construct(DB\SQL $db)
        {
            parent::__construct($db, 'bids');
        }
        
        function fetch_by_bids()
        {
            $this->load();
            return $this->query;
        }
        
        function fetch_previous_bid($item_id)
        {
            $row = $this->load(array('item_id=?', $item_id));
            return json_decode($row->bid_amount);
        }
        
        function update_bid($item, $user, $bid, $date_time)
        {
            $row = $this->load(array('item_id=?', $item));
            $row->user_id = $user;
            $row->bid_amount = $bid;
            $row->bid_dateTime = $date_time;
            $row->update();
        }
        
        
    }