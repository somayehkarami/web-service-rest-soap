<?php
    
    class BidsController extends Controller
    {
        
        private $model;
        
        function __construct()
        {
            parent::__construct();
            
            $this->model = new BidsModel($this->db);
        }
        
        function authenticate_bid($f)
        {
            $current_bid = $f->get('GET.bid_amount');
            $item_id = $f->get('GET.item_id');
            $user_id = $f->get('GET.user_id');
            
            $current_date_time = date("Y-m-d H:i:s");
            
            $previous_bid = $this->model->fetch_previous_bid($item_id);
            
            if ($current_bid > $previous_bid) {
                $this->model->update_bid($item_id, $user_id, $current_bid, $current_date_time);
                $f->reroute('/');
            } else {
                // return error message in the input placeholder
            }
        }
        
    }