<?php
    
    class RegisterController extends Controller
    {
        
        private $model;
        
        function __construct()
        {
            parent::__construct();
            
            $this->model = new UsersModel($this->db);
        }
        
        function registration()
        {
            $view = new Template;
            echo $view->render("register.html");
        }
        
        function add_user($f)
        {
            $email = $f->get('GET.user_email');
            $name = $f->get('GET.user_name');
            $display = $f->get('GET.user_display_name');
            if ($email != null && $name != null && $display != null) {
                $this->model->addUser($email, $name, $display);
                $user_id = $this->model->return_id($email);
                $f->set('COOKIE.user_id',$user_id);
                $f->set('display',$display);
                $view = new Template;
                $f->reroute("return/");
            } else {
                if ($email == null) {
                    $f->set('email_placeholder',"enter email");
                } else {
                    $f->set('email_value',$email);
                }
                if ($name == null) {
                    $f->set('name_placeholder',"enter name");
                } else {
                    $f->set('name_value',$name);
                }
                if ($display == null) {
                    $f->set('display_placeholder',"enter display name");
                } else {
                    $f->set('display_value',$display);
                }
    
                
                $view = new Template;
                echo $view->render("register.html");
            }
        }
    }