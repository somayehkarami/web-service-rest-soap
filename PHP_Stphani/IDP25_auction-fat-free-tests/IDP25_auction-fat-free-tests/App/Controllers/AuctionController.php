<?php
    
    class AuctionController extends Controller
    {
        
        private $model;
        
        function __construct()
        {
            parent::__construct();
            
            $this->model = new AuctionsModel($this->db);
        }
        
        function auction_page($f) // initialize my view
        {
            // grabs all of the items from the items table
            $f->set('auctions', $this->model->fetch_by_items());
            // to check and see if an item is still up for auction
            $f->set('current_time', date("Y-m-d H:i:s"));
            // set cookie to zero when first accessing the page
            $f->set('COOKIE.user_id', 0);
            $view = new Template;
            echo $view->render("auction_page.html"); // output the view
        }
        
        function return_to_auction_page($f)
        {
            // grabs all of the items from the items table
            $f->set('auctions', $this->model->fetch_by_items());
            // to check and see if an item is still up for auction
            $f->set('current_time', date("Y-m-d H:i:s"));
            
            $view = new Template;
            echo $view->render("auction_page.html"); // output the view
        }
        
        function fetchItems($f) //get the items
        {
            $template = new Template;
            echo $template->render("auction_page.html");
        }
        
        function logout($f)
        {
            $f->set('COOKIE.user_id', 0);
            $f->reroute("/");
        }
        
        function add_item($f)
        {
            $item_owner_id = $f->get('GET.item_owner_id');
            $item_name = $f->get('GET.item_name');
            $item_description = $f->get('GET.item_description');
            $item_due_date = date('Y-m-d H:i:s', strtotime($f->get('GET.item_due_date')));
            $item_bid_amount = $f->get('GET.item_bid_amount');
            $item_bidder_id = $f->get('GET.item_bidder_id');
            $item_bid_date = date('Y-m-d H:i:s');
            
            $this->model->add_item($item_owner_id, $item_name, $item_description, $item_due_date, $item_bid_amount, $item_bidder_id, $item_bid_date);
            
            $f->set('auctions', $this->model->fetch_by_items());
            
            $f->reroute('return/');
        }
        
        function delete_item($f, $param)
        {
            $this->model->delete_item($param['qid']);
            $f->reroute('return/');
        }
        
        function place_bid($f)
        {
            $item_id = $f->get('GET.item_id');
            
            $item_bid_amount = $f->get('GET.bid_amount');
            $item_bidder_id = $f->get('GET.COOKIE.user_id');
            $item_bid_date = date("Y-m-d H:i:s");
            
            
            $previous_bid = $this->model->find_previous_bid($item_id);
            
            if ($item_bid_amount > $previous_bid) {
                $this->model->update_bid($item_id, $item_bid_amount, $item_bidder_id, $item_bid_date);
                $f->reroute('return/');
            } else {
                // return error message in the input placeholder
            }
        }
    }