<?php
    
    class UserController extends Controller
    {
        
        private $model;
        
        function __construct()
        {
            parent::__construct();
            
            $this->model = new UsersModel($this->db);
        }
        
        function user_page($f)
        {
            $f->set('user_activity', $this->model->fetch_by_user_id(1)); // user variable
            // display user name "Welcome {{username}}"
            // for each loop from items where we fetch from items with user id
            // for each loop from bid where we fetch all bids with user id
            // and then display all of the items using item_id from items table
            $view = new Template;
            echo $view->render("user_page.html"); // output the view
        }
        
        
        
        
    }