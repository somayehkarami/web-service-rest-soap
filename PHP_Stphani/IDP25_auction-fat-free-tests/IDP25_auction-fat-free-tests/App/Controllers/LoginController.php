<?php
    
    class LoginController extends Controller
    {
        private $model;
        
        function __construct()
        {
            parent::__construct();
            
            $this->model = new UsersModel($this->db);
        }
        
        function login()
        {
            $view = new Template;
            echo $view->render("login_page.html");
        }
        
        function validate($f)
        {
            $name = $f->get('GET.user_name');
            $email = $f->get('GET.user_email');
            
            $id_by_name = $this->model->find_id_by_name($name);
            $id_by_email = $this->model->find_id_by_email($email);
            
            if ($id_by_name == $id_by_email) {
                $user_name = $this->model->return_display($email);
                $f->set('display',$user_name);
                $f->set('COOKIE.user_id',$id_by_name);
    
                $auction_table = new AuctionsModel($this->db);
                $f->set('auctions', $auction_table->fetch_by_items());
                $f->set('current_time', date("Y-m-d H:i:s"));
                
                $view = new Template;
                echo $view->render("auction_page.html");
            } else {
                $view = new Template;
                echo $view->render("login_page.html");
            }
        }
    }