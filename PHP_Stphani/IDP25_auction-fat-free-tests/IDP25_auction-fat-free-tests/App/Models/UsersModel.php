<?php
    
    class UsersModel extends DB\SQL\Mapper
    {
        function __construct(DB\SQL $db)
        {
            parent::__construct($db, 'users');
        }
        
        function fetch_by_user_id($id)
        {
            $this->load(array('user_id=?', $id));
            return $this->query;
        }
        
        function addUser($email, $name, $display)
        {
            $this->user_email = $email;
            $this->user_name = $name;
            $this->user_display_name = $display;
            $this->save();
        }
        
        function return_id($email)
        {
            $row = $this->load(array('user_email=?', $email));
            return json_encode($row->user_id);
        }
        
        function return_display_name($email)
        {
            $row = $this->load(array('user_email=?', $email));
            return json_encode($row->user_display_name);
        }
        
        function find_id_by_name($name)
        {
            $row = $this->load(array('user_name=?', $name));
            if ($row != null) {
                return json_encode($row->user_id);
            } else {
                // return variable with text containing message.
                echo "Invalid user name provided<br>";
            }
        }
        
        function find_id_by_email($email)
        {
            $row = $this->load(array('user_email=?', $email));
            if ($row != null) {
                return json_encode($row->user_id);
            } else {
                // return variable with text containing message.
                echo "\nInvalid email address provided<br>";
            }
        }
        
        function return_display($email)
        {
            $row = $this->load(array('user_email=?', $email));
            return json_encode($row->user_display_name);
        }
    }