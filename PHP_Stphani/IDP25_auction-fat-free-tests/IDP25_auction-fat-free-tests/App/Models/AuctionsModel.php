<?php
    
    class AuctionsModel extends DB\SQL\Mapper
    {
        
        function __construct(DB\SQL $db)
        {
            parent::__construct($db, 'items');
        }
        
        function fetch_by_items()
        {
            $this->load();
            return $this->query;
        }
        
        function add_item($owner, $name, $desc, $date, $bid, $bidder, $bid_date)
        {
            $this->item_owner_id = $owner;
            $this->item_name = $name;
            $this->item_description = $desc;
            $this->item_due_date = $date;
            $this->item_bid_amount = $bid;
            $this->item_bidder_id = $bidder;
            $this->item_bid_date = $bid_date;
            $this->save();
        }
        
        function delete_item($id)
        {
            $this->load(array('item_id=?', $id));
            $this->erase();
        }
        
        function find_previous_bid($item_id)
        {
            $row = $this->load(array('item_id=?', $item_id));
            return json_encode($row->item_bid_amount);
        }
        
        function update_bid($item_id, $item_bid_amount, $item_bidder_id, $item_bid_date)
        {
            $row = $this->load(array('item_id=?', $item_id));
            $row->item_bid_amount = $item_bid_amount;
            $row->item_bidder_id = $item_bidder_id;
            $row->item_bid_date = $item_bid_date;
            $row->update();
        }
    }