<!DOCTYPE html>
<html lang="en">

<?php echo $this->render('head.html',NULL,get_defined_vars(),0); ?>

    <body style="">
        <div class="container" style="">
            <header class="menu_container">
                <nav class="menu_look">
                    <a class="menu_button" href="#">Auction</a>
                </nav>
            </header>
            <main>
                <h1>Welcome <?= ($user_activity['user_display_name']) ?></h1>
                <div class="main_grid">

                    <div class="main_grid">
                        <div class="column">

                            <div id="new_item" class="">
                                <label class="input_label" for="item_name">Item Name:</label>
                                <input id="item_name" class="input_box" type="text">
                                <label class="input_label" for="item_description">Description:</label>
                                <input id="item_description" class="input_box" type="text">
                                <label class="input_label" for="item_due_date">Due Date:</label>
                                <input id="item_due_date" class="input_box" type="text">
                                <label class="input_label" for="item_initial_bid">Initial Bid:</label>
                                <input id="item_initial_bid" class="input_box" type="number">
                                <input type="submit" value="Create Item">
                            </div>

                            <div class="">
                                <tbody>
                                    
                                </tbody>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="main_grid">
                    <div class="main_grid">
                        <?php foreach (($user_bids?:[]) as $item): ?>
                            <tr>
                                <td>Item Name:</td>
                                <td><?= ($item['item_name']) ?></td>
                            </tr>
                            <tr>
                                <td>Item Description:</td>
                                <td><?= ($item['item_description']) ?></td>
                            </tr>
                            <tr>
                                <td>Due Date:</td>
                                <td><?= ($item['item_due_date']) ?></td>
                            </tr>
                            <tr>
                                <td>My Bid:</td>
                                <td><?= ($item['item_user_lastest_bid']) ?></td>
                            </tr>
                            <tr>
                                <td>Top Bid:</td>
                                <td><?= ($item['item_highest_bid']) ?></td>
                            </tr>
                            <tr>
                                <td class="button">Place Bid</td>
                                <td><input type="text" value="<?= ($item['item_bid']) ?>"></td>
                            </tr>
                        <?php endforeach; ?>
                    </div>
                </div>
            </main>
            <?php echo $this->render('footer.html',NULL,get_defined_vars(),0); ?>
        </div>
    </body>
</html>