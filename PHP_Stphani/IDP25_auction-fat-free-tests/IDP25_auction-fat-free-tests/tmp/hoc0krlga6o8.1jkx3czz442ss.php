<!-- Footer -->

<!DOCTYPE html>
<html lang="en">



<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <title>MSJS Auctions</title>

    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;700&display=swap">
    <!-- <link rel="stylesheet" type="text/css" href="assets/css/footer.css"> -->
    <style> .a-unstyle { text-decoration: none; } </style>

</head>




<body> 
 

<footer class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-2 mx-auto">
                <h5>About</h5>
                <ul class="nav flex-column">
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Partners</a></li>
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Services</a></li>
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Careers</a></li>
                </ul>
            </div>
        
            <div class="col-2 mx-auto">
                <h5>Resources</h5>
                <ul class="nav flex-column">
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Blog</a></li>
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Docs</a></li>
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Media</a></li>
                </ul>
            </div>
    
            <div class="col-2 mx-auto">
                <h5>Contact</h5>
                <ul class="nav flex-column">
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Help</a></li>
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Features</a></li>
                    <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">FAQs</a></li>
                </ul>
            </div>
    
            <div class="col-4 offset-1 mx-auto">
                <form>
                    <h5>Stay Updated</h5>
                    <p>Subscribe to our newsletter to get our latest news.</p>
                    <div class="d-flex w-100 gap-2" data-children-count="1">
                        <label for="newsletter1" class="visually-hidden">Email address</label>
                        <input id="newsletter1" type="text" class="form-control" placeholder="Enter email address" data-kwimpalastatus="alive" data-kwimpalaid="1628369631849-0" required>
                        <button class="btn btn-primary" type="button">Subscribe</button>
                    </div>
                </form>
            </div>
        </div>
  

        <div class="d-flex justify-content-between py-4 my-4 border-top">
            <p>© 2021 MSJS Auctions. All rights reserved.</p>
            <ul class="list-unstyled d-flex">
                <li class="social"><a href="#" class="a-unstyle" target="_blank"><img src="images/IG.png" alt="instagram logo"   width="25" height="25" />&nbsp</a>
                </li>
                <li class="social"><a href="#" class="a-unstyle" target="_blank"><img src="images/FB.png" alt="facebook logo"
                    width="25" height="25" />&nbsp</a>
                </li>
                <li class="social"><a href="#" class="a-unstyle" target="_blank"><img src="images/Twitter.png" alt="twitter logo"
                    width="25" height="25" />&nbsp</a>
                </li>
            </ul>
        </div>
    </div>
</footer>


  <!-- <script src="assets/bootstrap/js/bootstrap.min.js"></script> -->

</body>
</html>






<!-- 
        <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
            <div class="col-md-4 d-flex align-items-center">
                <a href="/" target="_blank">
                    <img src="images/auction.png" alt="instageram logo" width="24" height="24" />
                </a>
                <span class="text-muted">Copyright © 2021 MSJS Auctions, Inc</span>
            </div>

            <ul class="nav col-md-4 justify-content-end list-unstyled d-flex">
                <li><a href="#" target="_blank"><img src="images/IG.png"
                    alt="instageram logo" width="24" height="24" /></a></li>
                
                <li><a href="#" target="_blank"><img src="images/FB.png" alt="facebook logo"
                    width="24" height="24" /></a></li>
                
                <li><a href="#" target="_blank"><img src="images/Twitter.png" alt="twitter logo"
                    width="24" height="24" /></a></li>
            </ul>
        </footer>
    
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>







<body>
    <div class="container"></div>
    <footer>
        <link rel="stylesheet" type="text/css" href="assets/css/footer.css" /> 
            <div class="row">
                <div class="col-4">
                    <
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Promotions</a></li>
                    </ul>
                </div>
                <div class="col-4">
                    <ul>
                        <li><a href="#">Partners</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">FAQs</a></li>
                        <li><a href="#">Media</a></li>
                    </ul>
                </div>
                <div class="col-4">
                    <h3>Social</h3>
                    <ul>
                        <li><a href="#" target="_blank"><img src="images/IG.png"
                                    alt="instageram logo" width="25" height="25" /></a></li>
                        <li><a href="#" target="_blank"><img src="images/FB.png" alt="facebook logo"
                                    width="25" height="25" /></a></li>
                        <li><a href="#" target="_blank"><img src="images/Twitter.png" alt="twitter logo"
                                    width="25" height="25" /></a></li>
                    </ul>
                </div>
            </div>

            <p>Copyright &copy; 2021 MSJS Auctions Inc. &middot;
                <a href="#">Privacy</a> &middot;
                <a href="#">Terms &amp Conditions</a><br><br>
            </p>
    </footer>
</body> -->
    

