<!-- Footer -->
 
    <footer class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-2 mx-auto">
                    <h5>About</h5>
                    <ul class="nav flex-column">
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Partners</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Services</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Careers</a></li>
                    </ul>
                </div>
            
                <div class="col-2 mx-auto">
                    <h5>Resources</h5>
                    <ul class="nav flex-column">
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Blog</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Docs</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Media</a></li>
                    </ul>
                </div>
        
                <div class="col-2 mx-auto">
                    <h5>Contact</h5>
                    <ul class="nav flex-column">
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Help</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Features</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">FAQs</a></li>
                    </ul>
                </div>
        
                <div class="col-4 offset-1 mx-auto">
                    <form>
                        <h5>Stay Updated</h5>
                        <p>Subscribe to our newsletter to get our latest news.</p>
                        <div class="d-flex w-100 gap-2" data-children-count="1">
                            <label for="newsletter1" class="visually-hidden">Email address</label>
                            <input id="newsletter1" type="text" class="form-control" placeholder="Enter email address" data-kwimpalastatus="alive" data-kwimpalaid="1628369631849-0" required>
                            <button class="btn btn-primary" type="button">Subscribe</button>
                        </div>
                    </form>
                </div>
            </div>
    

            <div class="d-flex justify-content-between py-4 my-4 border-top">
                <p>© 2021 MSJS Auctions. All rights reserved.</p>
                <ul class="list-unstyled d-flex">
                    <li class="social"><a href="#" class="a-unstyle" target="_blank"><img src="images/IG.png" alt="instagram logo"   width="25" height="25" />&nbsp</a>
                    </li>
                    <li class="social"><a href="#" class="a-unstyle" target="_blank"><img src="images/FB.png" alt="facebook logo"
                        width="25" height="25" />&nbsp</a>
                    </li>
                    <li class="social"><a href="#" class="a-unstyle" target="_blank"><img src="images/Twitter.png" alt="twitter logo"
                        width="25" height="25" />&nbsp</a>
                    </li>
                </ul>
            </div>
        </div>
    </footer>


    <script src="assets/bootstrap/js/bootstrap.min.js"></script>

    </body>

</html>
