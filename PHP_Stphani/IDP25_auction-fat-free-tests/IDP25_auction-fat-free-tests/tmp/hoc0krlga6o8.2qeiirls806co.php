<!DOCTYPE html>
<html lang="en">

<?php echo $this->render('head.html',NULL,get_defined_vars(),0); ?>

    <body style="">
        <div class="container" style="">

            <header class="menu_container">
                <nav class="menu_look">
                    <a class="menu_button" href="#">LogIn</a>
                </nav>
            </header>
            <main>
                <!--slap a repeat here-->
                <article class="item_block">
                    <div class="item_details">
                        <div class="item_name">Nut Cracker</div>
                        <div class="item_description">Cracks nuts like nothing else you've ever seen. Crack nuts, make
                            friends.
                        </div>
                        <div class="item_due_date">August 20, 2021</div>
                        <div class="item_initial_bid">300</div>
                        <div class="item_owner">B. Breaker Barnum</div>
                    </div>
                    <div class="bids">
                        <div class="bid01">
                            <div class="bid_number">12</div>
                            <div class="bid_time">10 minutes ago</div>
                            <div class="bid_amount">700</div>
                            <div class="bid_owner">Brian Barett</div>
                        </div>

                    </div>
                </article>
                <?php foreach (($auctions?:[]) as $item): ?>
                    <article class="item_block">
                        <div class="item_details">
                            <div class="item_name"><?= ($item['item_name']) ?></div>
                            <div class="item_description"><?= ($item['item_description']) ?></div>
                            <div class="item_due_date"><?= ($item['item_due_date']) ?></div>
                            <div class="item_initial_bid"><?= ($item['item_initial_bid']) ?></div>
                            <div class="item_owner"><?= ($item['item_owner_id']) ?></div>
                        </div>
                        <div class="bids">
                        </div>
                    </article>
                <?php endforeach; ?>

            </main>
            <?php echo $this->render('footer.html',NULL,get_defined_vars(),0); ?>
        </div>
    </body>

</html>