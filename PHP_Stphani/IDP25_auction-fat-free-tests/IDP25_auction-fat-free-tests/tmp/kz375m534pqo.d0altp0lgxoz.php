<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <title>MSJS Auctions - Login</title>

    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;700&display=swap">
</head>


<body>

<div class="container">
	<header class="d-flex flex-wrap align-items-center justify-content-center 		justify-content-md-between py-3 mb-4 border-bottom">
		  	<a href="/" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark 		text-decoration-none">
				<img src="images/MSJSdesign.png" alt="MSJS logo" width="75" height="75" />
			</a>
	
		<ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
			<li><a href="#" class="nav-link px-2 link-dark" style="font-size: large;">MSJS Auctions</a></li>
		
		</ul>
	
		<div class="col-md-3 text-end">
			<!-- Removed the Login Button as we are on the Login page -->
			<button type="button" class="btn btn-primary">Sign-up</button>
		</div>
	</header>
</div>
