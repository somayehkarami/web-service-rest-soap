<!DOCTYPE html>
<html lang="en">

<?php echo $this->render('head.html',NULL,get_defined_vars(),0); ?>

    <body style="">
        <div class="container" style="">

            <!-- Log In or Log Out is displayed depending on the cookie's user_id setting -->
            <header class="menu_container">
                <nav class="menu_look">
                    <?php if ($COOKIE['user_id'] == 0): ?>
                        
                            <a class="menu_button" href="login">LogIn</a>
                        
                        <?php else: ?>
                            <a class="menu_button" href="logout">LogOut</a>
                        
                    <?php endif; ?>
                </nav>
            </header>
            <main>
                <!-- Prints "Hello Stranger" if no one is logged in, else prints "Hello (Display Name)" -->
                <div class="header">
                    <?php if ($COOKIE['user_id'] == 0): ?>
                        
                        
                        <?php else: ?>
                            <form action="add_item" method="GET">
                                <input type="hidden" name="item_owner_id" value="<?= ($COOKIE['user_id']) ?>">

                                <label class="input_label" for="item_name">Name:</label>
                                <input id="item_name" name="item_name" class="input_box" type="text">

                                <label class="input_label" for="item_description">Description:</label>
                                <input id="item_description" name="item_description" class="input_box" type="text">

                                <label class="input_label" for="item_due_date">Expiration Date:</label>
                                <input id="item_due_date" name="item_due_date" class="input_box" type="datetime-local">

                                <label class="input_label" for="item_bid_amount">Starting Bid:</label>
                                <input id="item_bid_amount" name="item_bid_amount" class="input_box" type="text">

                                <input class="button" type="submit" value="Submit">
                            </form>
                        
                    <?php endif; ?>
                </div>


                <!--slap a repeat here-->
                <article class="item_block">
                    <div class="item_details">
                        <div class="item_name">Nut Cracker</div>
                        <div class="item_description">Cracks nuts like nothing else you've ever seen. Crack nuts, make
                            friends.
                        </div>
                        <div class="item_due_date">August 20, 2021</div>
                        <div class="item_initial_bid">300</div>
                        <div class="item_owner">B. Breaker Barnum</div>
                    </div>
                    <div class="bids">
                        <div class="bid01">
                            <div class="bid_number">12</div>
                            <div class="bid_time">10 minutes ago</div>
                            <div class="bid_amount">700</div>
                            <div class="bid_owner">Brian Barett</div>
                        </div>

                    </div>
                </article>
                <?php foreach (($auctions?:[]) as $item): ?>
                    <article class="item_block">
                        <div class="item_grid">
                            <div class="item_label">Owner:</div>
                            <div class="item_owner"><?= ($item['item_owner_id']) ?></div>

                            <div class="item_label">Name:</div>
                            <div class="item_detail"><?= ($item['item_name']) ?></div>

                            <div class="item_label">Description:</div>
                            <div class="item_description"><?= ($item['item_description']) ?></div>

                            <div class="item_label">Due Date:</div>
                            <div class="item_due_date"><?= ($item['item_due_date']) ?></div>

                            <div class="item_label">Current Bid:</div>
                            <div class="item_initial_bid"><?= ($item['item_bid_amount']) ?></div>

                            <div class="item_label">Bid Date:</div>
                            <div class="item_initial_bid"><?= ($item['item_bid_date']) ?></div>

                            <?php if ($item['item_due_date'] > $current_time && $COOKIE['user_id'] != 0
                            && $item['item_owner_id'] != $COOKIE['user_id']): ?>
                                
                                    <form method="GET" action="place_bid">
                                        <input type="submit" value="Place Bid">
                                        <input type="number" name="bid_amount" placeholder="enter amount" value="bid_amount" required>
                                        <input type="hidden" name="item_id" value="<?= ($item['item_id']) ?>">
                                        <input type="hidden" name="user_id" value="<?= ($COOKIE['user_id']) ?>">
                                    </form>
                                
                                <?php else: ?>
                                    <span></span>
                                
                            <?php endif; ?>
                            <?php if ($COOKIE['user_id'] == $item['item_owner_id']): ?>
                                
                                    <form action="delete_item/<?= ($item['item_id']) ?>" method="GET">
                                        <input type="submit" value="Remove">
                                    </form>
                                
                                <?php else: ?>
                                    <span></span>
                                
                            <?php endif; ?>
                        </div>
                        <div class="bids">
                        </div>
                    </article>
                <?php endforeach; ?>

            </main>
            <?php echo $this->render('footer.html',NULL,get_defined_vars(),0); ?>
        </div>
    </body>

</html>