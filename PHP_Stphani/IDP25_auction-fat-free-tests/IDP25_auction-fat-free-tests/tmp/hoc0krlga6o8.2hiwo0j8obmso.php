<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <title>MSJS Auctions</title>

    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;700&display=swap">
    <!-- <link rel="stylesheet" type="text/css" href="assets/css/head.css"> -->
</head>


<body>
    <!-- <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <div class="container-fluid"><a class="navbar-brand" href="#">MSJS Auctions</a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

                <?php if ($isLoggedIn): ?>
                    <div class="alert alert-danger" role="alert"><?= ($isLoggedIn) ?></div>
                <?php endif; ?>

                <div class="navbar-collapse collapse" id="navbarCollapse">
                    <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                    </li>
                    </ul> 
                    <form class="d-flex" data-children-count="1">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                    </form>
                    <span class="navbar-text"><a class="login" href="login_page" style="font-size: 23px;">Log In</a></span><a class="btn btn-light action-button" role="button" href="create_user_page" style="font-size: 23px;">Sign Up</a>
                </div>
            </div>
          </nav>
    </header> -->



<div class="container">
	<header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-bottom">
		  	<a href="/" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark 		text-decoration-none">
				<img src="images/MSJSdesign.png" alt="MSJS logo" width="75" height="75" />
			</a>
	
		<ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
			<li><a href="#" class="nav-link px-2 link-dark" style="font-size: large;">MSJS Auctions</a></li>
		
		</ul>
	
		<div class="col-md-3 text-end">
			<button type="button" class="btn btn-outline-primary me-2">Login</button>
			<button type="button" class="btn btn-primary">Sign-up</button>
		</div>
	</header>
</div>


