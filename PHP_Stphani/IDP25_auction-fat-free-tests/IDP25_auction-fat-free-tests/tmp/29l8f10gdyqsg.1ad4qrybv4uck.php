<!DOCTYPE html>
<html lang="en">

<body>
    <main>
        <form action="register" method="GET">
            <label class="input_label" for="user_email">Email:</label>
            <input id="user_email" name="user_email" class="input_box" type="email"
                   placeholder="<?= ($email_placeholder) ?>" value="<?= ($email_value) ?>" required>

            <label class="input_label" for="user_name">Username:</label>
            <input id="user_name" name="user_name" class="input_box" type="text"
                   placeholder="<?= ($name_placeholder) ?>" value="<?= ($name_value) ?>" required>

            <label class="input_label" for="user_display_name">Display Name:</label>
            <input id="user_display_name" name="user_display_name" class="input_box" type="text"
                   placeholder="<?= ($display_placeholder) ?>" value="<?= ($display_value) ?>" required>

            <input class="button" type="submit" value="Submit">
        </form>
    </main>

</body>
</html>