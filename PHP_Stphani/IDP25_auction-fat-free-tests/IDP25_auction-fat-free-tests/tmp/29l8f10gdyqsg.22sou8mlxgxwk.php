<!DOCTYPE html>
<html lang="en">
<?php echo $this->render('head.html',NULL,get_defined_vars(),0); ?>
    <body>

        <main>

            <form action="validation" method="GET">
                <label class="input_label" for="user_name">Username:</label>
                <input id="user_name" name="user_name" class="input_box" type="text" required>
                <label class="input_label" for="user_email">Email:</label>
                <input id="user_email" name="user_email" class="input_box" type="email" required>
                <input class="button" type="submit" value="Submit">
            </form>
            <div class="button">
                <a type="button" href="go_to_register">Register</a>
            </div>
        </main>

        <?php echo $this->render('footer.html',NULL,get_defined_vars(),0); ?>

    </body>
</html>