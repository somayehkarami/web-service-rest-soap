<?php
    
    require "vendor/autoload.php";
    // require "App/Controllers/QuestionsController.php";
    
    // initialize fatfree
    $f3 = Base::instance();
    
    // config
    $f3->set("AUTOLOAD", "App/Controllers/,App/Models/");
    $f3->set("UI", "App/Views/");
    
    // routes
    $f3->route("GET /", "AuctionController->auction_page");
    $f3->route("GET /return", "AuctionController->return_to_auction_page");
    
    $f3->route("GET /user", "UserController->user_page");
    $f3->route("GET /login", "LogInController->login");
    $f3->route("GET /logout", "AuctionController->logout");
    $f3->route("GET /validation", "LoginController->validate");
    // goto register brings user to the registration page
    $f3->route("GET /go_to_register", "RegisterController->registration");
    // register validates the user registration credentials
    $f3->route("GET /register", "RegisterController->add_user");
    $f3->route("GET /add_item", "AuctionController->add_item");
    $f3->route("GET /delete_item/@qid", "AuctionController->delete_item");
    $f3->route("GET /place_bid", "AuctionController->place_bid");
    
    // keeping the debug level to this 'cause we're cool like that
    $f3->set("DEBUG",2);
    
    // execute fatfree
    $f3->run();
