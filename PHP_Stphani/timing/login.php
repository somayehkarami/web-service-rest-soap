<?php  include "function.php";



//check if user is already logged in
if(isLoggedIn()){
	header('Location: index.php');
  }

$data_code = "";
$data_password="";

//$now = time();

//Form was submited
if($_SERVER['REQUEST_METHOD'] == "POST"){

	/*  ***********************************  Code *************************** */
	//validate the form  data
	if (validateData($_POST['access_code']) ){
		$data_code  = $_POST['access_code'];
	}else{//error
	  $errors [] ="Code is not valid";
  
	}
  
	 /*  ***********************************  Password *************************** */
	//validate the data
	if (validateData($_POST['password']) ){
		$data_password  = $_POST['password'];
	}else{//error
	  $errors [] ="Password is not valid";
  
	}

	$now = floor(time());

	if(count($errors) ==0){
	

        $results = DB:: insert ("timing_users",['access_code' =>$data_code, 'password'=>
		$data_password, 'login_time'=>$now]);


    //if(count ($result)==0){//nothing found in array
	if(is_null($results)){//nothing found in array-if is null return 1.
		$errors[] = "Invalid login credentials";
	  }else{

		//set cookies to remember user
		//$expiration = time()+(20 * 60 * 60);//(time)
		//setcookie("cookie_name",$data_code, $expiration);//name of cookie , value , how much time to expair
	   

		
      //session_start(); 
      $_SESSION["session_name"] = $data_code;
	  $_SESSION['login_time'] = time();
      //header('Location: index.php');//- Redirect to index listing
	  }
	}

	 

	session_start();
	if(isset($_SESSION["session_name"])){
		if((time()- $_SESSION['login_time'])>120){
		 header('Location: index.php');
		}
	else {
	 header('Location: logout.php');
	
	}
 }

     

}//=====


?>


<!DOCTYPE html>
<html>
<head>
	<title>Login - Test 2</title>
	<!--Bootsrap 4 CDN-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div class="card-header">
				<h3>Access to Test #2</h3>
			</div>
			<div class="alert alert-danger" id="alert" role="alert">
				<!--An Error Message Here!-->
				<?php //error checking
                   displayError($errors);
                ?>
			</div>
			<div class="card-body">
				<form class="" method="POST" >
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-hashtag"></i></span>
						</div>
						<input type="text" class="form-control" placeholder="access code" name="access_code">
						
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" class="form-control" placeholder="password" name="password">
					</div>
					<div class="form-group">
						<input type="submit" value="Login" class="btn float-right login_btn">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>