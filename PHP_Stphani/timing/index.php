<?php  include "function.php";

// redirect   user
//header('Location: login.php');


$results_qu = DB::query("SELECT * FROM timing_quotes");

/*$storeArray = Array();
while ($row = mysql_fetch_array($results_qu, MYSQL_ASSOC)) {
    $storeArray[] =  $row['quote'];  
}*/

?>

<!DOCTYPE html>
<html>
<head>
	<title>Timing Quotes - Test 2</title>
	<!--Bootsrap 4 CDN-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div class="card-header">
				<h3>You have 000 seconds left!
				
                 
				</h3>
			</div>

			<div class="card-body">
				<blockquote class="blockquote">
					<!--<p class="mb-1">Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live.</p>-->
					
					<?php foreach($results_qu as $result){?>
						<p class="mb-1" > <?php $result['quote'][rand(0, strlen($result))] ?> </p> 
						<?php } ?>
			
			
					<!--<footer class="text-right blockquote-footer">Martin Golding</footer>-->
					<?php foreach($results_qu as $result){?>
					<footer class="text-right blockquote-footer"><?php $result['author'] ?></footer>
					<?php } ?>
				</blockquote>

			</div>
		</div>
	</div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>