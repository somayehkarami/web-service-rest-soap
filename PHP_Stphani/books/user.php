<?php  include "includes/function.php";

$data_name ="";
$data_password="";


if($_SERVER['REQUEST_METHOD'] == "POST"){

  /*  ***********************************  username *************************** */
  //validate the data
  if (validateData($_POST['name'])){
    $data_name = $_POST['name'];
  }else{//error
    $errors [] ="Name is not valid";

  }
  

   /*  ***********************************  Password *************************** */
  //validate the data
  if (validateData($_POST['password'])){
    $data_password = $_POST['password'];
  }else{//error
    $errors [] ="Password is not valid";

  }
}

if(count($errors )== 0){
  DB:: insert ("users",['username'=> $data_name, 'password'=>$data_password]);  
  DB::query("UPDATE users SET username=%s, password=%i WHERE ID=%i",$data_name, $data_password,$_POST['id']);
}

//redirect into database
//header("Location: users.php");


 include "includes/header.php"; 
 ?>
      <div class="row">
        <div class="col-12">
          <h2>Bookstore User</h2>
          <hr class="mb-4"> </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-success">
            <h4 class="alert-heading">Success!</h4>
            <p class="mb-0">User has been updated</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 p-3">
          <form action="" method="POST">
            <div class="form-group">
              <label>Name</label>
              <input type="text" class="form-control" name="name" value="">
            </div>
          <!--  <div class="form-group">
              <label>Email</label>
              <input type="text" class="form-control" name="email" value="">
            </div> -->
            <div class="form-group">
              <label>Password</label>
              <input type="password" class="form-control" name="password" value="">
            </div>
            <input type="hidden" name="ID" value="<?php echo $info['ID']; ?>" /><br />
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        

</body>

</html>
<?php include "includes/footer.php"; // include or copy/paste file contents
?>
