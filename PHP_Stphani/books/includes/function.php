<?php

//composer autoloader
require("vendor/autoload.php");//I need to access any file that you have



//MEEKRO VARIABLE
DB::$user = 'ipd25';
DB::$password = 'ipd25_pw';
DB::$dbName = 'ipd25_class';

//session start
session_start();
//varibles
$errors=[];



//Functions
//This function returns true if the variable exists and is not NULL,
// otherwise it returns false.
function validateData($data){
    $is_valid = false;
    //Also check whether the variable is set
    if(isset ($data)&& $data !=""){
      
    $is_valid = true;
}
return $is_valid ;
}

//retrive the name of the author from a gievn id
function getAuthorName($author_id){
   $results =DB::queryFirstField("SELECT username FROM users WHERE id=%i LIMIT 1", $author_id);
    return $results ;
}


//check if a user is logged in
function isLoggedIn(){
  $loggedIn = false;

  //does cookie_username exist and has content
  //does session_username exist and has contnet
  if( !empty($_COOKIE['cookie_username']) && validateData($_COOKIE['cookie_username']) 
  && !empty($_SESSION['session_username'])&& validateData($_SESSION['session_username'])){
  
     $loggedIn =true;
  }
  return $loggedIn;
}


//show the errors on the screen
function displayError($list){
     if(count($list)>0){ 
        echo  "<div class ='alert alert-danger'>";
        foreach($list as $error) echo $error . "<br />";
          echo "</div>";

      }//end of count errors 
}


?>

