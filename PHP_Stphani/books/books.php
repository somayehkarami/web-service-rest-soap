
<?php  include "includes/function.php";

//add header****************************
 include "includes/header.php"; 

 //add some style to table in header.php file!!!!!

 /* ****************** write some queries**************************** */
 $results = DB::query("SELECT * FROM books");
 //$results = DB::query("SELECT	*	FROM	books WHERE pub_year <= %i",2020);
 //$results = DB::query("SELECT	*	FROM	books WHERE description LIKE %ss","Java");
 //$results = DB::query("SELECT	*	FROM	books WHERE author = %i",1);
 //$results = DB::query("SELECT	*	FROM	books WHERE id = %i",4);
 

 ?>
      <div class="row">
        <div class="col-12">
          <h2>Bookstore Books</h2>
          <hr class="mb-4"> </div>
      </div>
      <div class="row">
        <div class="col-md-12 p-3">
          <table class="table table-hover table-striped table-bordered ">
            <thead class="thead-dark">
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Author</th>
                <th>Year</th>
                <th>Editor</th>
                <th>Description</th>
                <th></th>
              </tr>
              <!-- Show data from database into html table in file books.php-->
             <?php
             if(count($results)==0){
              echo "There were no results";
            }else{//there is results

              foreach	($results as $row)	{?>
              <tr>
                <td><?php echo $row['id'] ;?></td>
                <td><?php echo $row['title']; ?></td>
                <td><?php echo getAuthorName($row['author']); ?></td>
                <td><?php echo $row['pub_year']; ?></td>
                <td><?php echo $row['editor']; ?></td>
                <td><?php echo $row['description']; ?></td>
              </tr>
             <?php
              }
           }
              ?>
            </thead>
            <tbody>

            </tbody>
          </table>      
</body>
</html>


<!--add footer***********-->
<?php include "includes/footer.php"; 
?>