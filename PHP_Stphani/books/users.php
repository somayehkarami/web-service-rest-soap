
<?php  include "includes/function.php";

$data_name ="";
$data_password="";
$data_confirm = "";


$results = DB::query("SELECT * FROM users");
if($_SERVER['REQUEST_METHOD'] == "POST"){

  /*  ***********************************  username *************************** */
  //validate the data
  if (validateData($_POST['name'])){
    $data_name = $_POST['name'];
  }else{//error
    $errors [] ="Name is not valid";

  }
  

   /*  ***********************************  Password *************************** */
  //validate the data
  if (validateData($_POST['pword'])){
    $data_password = $_POST['pword'];
  }else{//error
    $errors [] ="Password is not valid";

  }

   /*  ***********************************  Confirm Password *************************** */
  //validate the data
  if (validateData($_POST['confirm']) ){
    $data_confirm = $_POST['confirm'];
  }else{//error
    $errors [] =" Confirm Password is not valid";

  }

  
}
if ($data_password  !== $data_confirm) {
  die('Password and Confirm password should match!');   
}else{


if(count($errors )== 0){
   DB:: insert ("users",['username'=> $data_name, 'password'=>$data_password]);  
  
}
}

$results = DB::query("SELECT * FROM users");
include "includes/header.php"; // include or copy/paste file contents
?>
      <div class="row">
        <div class="col-12">
          <h2>Create User</h2>
          <hr> </div>
      </div>
      <div class="row">
        <div class="col-sm-12 col-md-6 offset-md-3">
          <form class="" method="POST">
            <div class="form-group">
              <label>Name</label>
              <input type="text" class="form-control" name="name">
            </div>
           <!-- <div class="form-group">
              <label>Email Address</label>
              <input type="email" class="form-control" name="email">
            </div>-->
            <div class="form-group">
              <label>Password</label>
              <input type="password" class="form-control" name="pword">
              <small class="form-text text-muted"></small>
            </div>
            <div class="form-group">
              <label>Confirm Password</label>
              <input type="password" class="form-control" name="confirm">
              <small class="form-text text-muted"></small>
            </div>
            <button type="submit" class="btn btn-primary">Save My Info</button>
          </form>
        </div>
      </div>
      <div class="row mt-4">
        <div class="col-12">
          <h2>List All User</h2>
          <hr>
        </div>
        <div class="table-responsive col-12">
          <table class="table table-hover table-striped table-bordered">
            <thead class="thead-dark">
              <tr>
                <th>ID</th>
                <th>Name</th>
                <!--<th>Email</th>-->
                <th>Password</th>
                <th></th>
              </tr>
              <!-- Show data from database into html table in file books.php-->
             <?php
             if(count($results)==0){
              echo "There were no results";
            }else{//there is results

              foreach	($results as $row)	{?>
              <tr>
                <td><?php echo $row['id'] ;?></td>
                <td><?php echo $row['username']; ?></td>
                <td><?php echo $row['password']; ?></td>
                </tr>
             <?php
              }
           }
              ?>
            </thead>
            <tbody>
            
            </tbody>
          </table>
      
</body>

</html>
<?php include "includes/footer.php"; // include or copy/paste file contents
?>