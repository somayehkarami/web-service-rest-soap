
<?php  include "includes/function.php";



//check if user is already logged in
if(isLoggedIn()){
  header('Location: books.php');
}

$username = "";
$password="";


//Form was submited
if($_SERVER['REQUEST_METHOD'] == "POST"){

  /*  ***********************************  Username *************************** */
  //validate the form  data
  if (validateData($_POST['username'])){
    $username  = $_POST['username'];
  }else{//error
    $errors [] ="Username is not valid";

  }

   /*  ***********************************  Password *************************** */
  //validate the data
  if (validateData($_POST['password'])){
    $password  = $_POST['password'];
  }else{//error
    $errors [] ="Password is not valid";

  }
//===============================================================
   //3. On error, show error to user
   //- invalid combination (does it exists in db)

  if(count($errors) ==0){
    $results = DB::queryFirstRow("SELECT * FROM users WHERE username = %s 
    AND password = %s",$username,$password);

    //if(count ($result)==0){//nothing found in array
      if(is_null($results)){//nothing found in array-if is null return 1.
      $errors[] = "Invalid login credentials";
    }else{

       //4. No error
      //- ** Login the user!
     //- Redirect to books listing

      //set cookies to remember user
      $expiration = time()+(10 * 60 * 60);//(time)
      setcookie("cookie_username",$username, $expiration);//name of cookie , value , how much time to expair
     
      //set session to remember user 
      //session_start(); add to function.php
      $_SESSION["session_username"] = $username;
      header('Location: books.php');//- Redirect to books listing
      //die();
      //- ** Login the user!
     

    }
  }


//==================================================================   

}

//add header****************************
 include "includes/header.php"; 
 ?>
      <div class="row">
        <div class="col-12">
          <h2>Login</h2>
          <hr> 
        <?php //error checking
         displayError($errors);
         ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 col-md-6 offset-md-3">
          <form class="" method="POST">
            <div class="form-group">
              <label>UserName</label>
              <input type="text" class="form-control" name="username"> </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" class="form-control" name="password">
              <small class="form-text text-muted"></small>
            </div>
            <button type="submit" class="btn btn-primary">Log Me In</button>
          </form>
       

</body>

</html>
<!--add footer***********-->
<?php include "includes/footer.php"; 
?>