<?php  include "includes/function.php";

if( !isLoggedIn()){
  header('Location: login.php');
}

 $data_title = "";
 $data_author="";
 $data_editor="";
 $data_description="";
 $data_pup_year="";


//Form was submited
if($_SERVER['REQUEST_METHOD'] == "POST"){

/*  ***********************************  Title *************************** */
  //validate the data
  if (validateData($_POST['name'])){
    $data_title = $_POST['name'];
  }else{//error
    $errors [] ="Title is not valid";

  }


/*  ****************************************  Author  *********************** */
if (validateData($_POST['author'])){
  $data_author = $_POST['author'];
}else{//error
  $errors [] ="Author is not valid";

}
/*  ****************************************  Description  *********************** */
if (validateData($_POST['description'])){
  $data_description = $_POST['description'];
}else{//error
  $errors [] ="Description is not valid";

}
/*  ****************************************  Editor *********************** */
if (validateData($_POST['editor'])){
  $data_editor = $_POST['editor'];
}else{//error
  $errors [] ="Editor is not valid";

}

/*  **************************************** pub_year  *********************** */
if (validateData($_POST['year_pub'])){
  $data_pup_year = $_POST['year_pub'];
}else{//error
  $error [] ="year_pub is not valid";

}

}//if($_SERVER['REQUEST_METHOD'] == "POST")



/* **************************************Insert to database************************ */
//check there is no error before inserting the data
if(count($errors )== 0){
  DB:: insert ("books",['title'=> $data_title, 'editor'=>$data_editor, 'author' =>$data_author,
  'description' => $data_description, 'pub_year'=> $data_pup_year] );
}

//redirect into database
//header("Location: books.php");

//retrieve list of authors
$results_authors = DB::query("SELECT * FROM users");


 include "includes/header.php"; // include or copy/paste file contents
?>


      <div class="row">
        <div class="col-12">
          <h2>Bookstore Book</h2>
          <hr>
          <?php  /*if(count($errors)>0){ 
            echo  "<div class ='alert alert-danger'>";
            foreach($errors as $error) echo $error . "<br />";
              echo "</div>";

          }//end of count errors */
          displayError($errors);
          ?>
          
          <div class="col-sm-12 col-md-8 offset-md-2">
            <form action="" method="POST">
              <div class="form-group">
                <label>Book Title</label>
                <input type="text" class="form-control" name="name" value="<?php  echo $data_title; ?>">
              </div>
              <div class="form-group">
                <label>Author</label>
                <select class="form-control" name="author" />
                  <option value="">----</option>
                  <?php foreach($results_authors as $author){
                     //set defalut value
                     $selected =($author == $data_author)? "selected" : "";
                    echo "<option value='" .$author['id'] .  "'$selected  >" . $author['username'] 
                    ."</option>";
                  }?>
                </select>
              </div>
              <div class="form-group">
                <label>Year of Publication</label>
                <select class="form-control" name="year_pub" />
                    <option value="">----</option>
                    <?php 
                    for($y = date('Y');$y>= (date('Y')-20);$y--){
                      //set defalut value
                      $selected = ($y == $data_pub_year)?"selected" : "";
                      echo "<option value ='$y' $selected>$y </option>";
                    }

                  ?>
                  </select>
              </div>
              <div class="form-group">
                <label>Editor</label>
                <input type="text" class="form-control" name="editor" value="<?php  echo $data_editor; ?>">
              </div>
              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" rows="3" name="description"><?php  echo $data_description; ?></textarea>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
              <input type="hidden" name="id" value="" />
            </form>
          </div>
 
</body>
 
</html>
 
<?php include "includes/footer.php"; // include or copy/paste file contents
?>