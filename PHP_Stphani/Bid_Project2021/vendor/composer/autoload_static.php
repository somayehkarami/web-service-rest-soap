<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit608e701af786d3f95c3535e083d9cf83
{
    public static $files = array (
        '45e8c92354af155465588409ef796dbc' => __DIR__ . '/..' . '/bcosca/fatfree/lib/base.php',
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInit608e701af786d3f95c3535e083d9cf83::$classMap;

        }, null, ClassLoader::class);
    }
}
