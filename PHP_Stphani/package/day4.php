<?php
$str = "<h1>I am learning PHP Programing</h1>";
echo $str;
// initialize variables
$txt_animals = "";
$txt_count = "";
$arr_animals = ["dog", "cat", "monkey", "hamster"];
$txt_error = "";

//check if my form was submitted
if ( $_SERVER['REQUEST_METHOD'] == "POST" ){
   
   // does "animals" index exists in $_POST (isset)
  // does the $_POST['animals'] value exists in array listing
   if(isset($_POST['animals']) && in_array($_POST['animals'],$arr_animals)){
       $txt_animals = $_POST['animals'];
   }else {
    //failed animals
    $txt_error = "Animals are not valid";
    
}
// does "count" index exists in $_POST AND is it an integer above 0
if ( isset ( $_POST['count'] ) && 
filter_var( $_POST['count'], FILTER_VALIDATE_INT, ["options"=>["min_range"=>0]] ) !== false ) {
// validate if number
$txt_count = $_POST['count'];
} else {
//failed count
$txt_error = "Count is not valid";
}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    
    <title>Animals</title>
</head>
<body>
<h3>
		<?php 
			// show date and time 
			$time = time();
			echo "The current time is " . date("H:ia", $time);
		?>
	</h3>

<form action="day4.php" method="POST">
<input type="hidden" name="words" value="cats"/>
<label for="txtAnimal">How many Monkeys</label>
<select name="animals" id="txtAnimals">
<?php  
foreach($arr_animals as $item){
    // if ($item == $txt_aniamls){ $selected == "selected"; } else { $selected = ""; }
    $selected = ($item == $txt_animals) ? "selected" : "";
    echo "<option value=\"$item\" $selected >$item</option>";
}
?>
<!--<option value="dog">Dogs</option>
<option value="cat">Cats</option>
<option value="monkey">Monkeys</option> -->
</select>
<input type="text" name="count" value="<?php echo $txt_count; ?>" placeholder="number of aniamls" />
<button type="submit" name="btnSubmit" value="btn1">Show My Monkeys</button>
</form>
<hr>
<div>
		<?php if ( $txt_animals != "" && $txt_count != ""){
		// $plural = ( $txt_count != 1 ) ? "s" : "";
		// echo "There are $txt_count $txt_animals$plural!"; 
		// forgot about making the good english ;)
		if ( $txt_count == 1) {
			echo "There is 1 $txt_animals!";
		} else {
			echo "There are " . $txt_count. " " . $txt_animals ."s!";
		}
	} 
    
    //time
    $nextWeek = time() + (60 * 60 * 24 * 7);
    echo date("d-m-Y", $nextWeek);
    echo "<hr>";
     echo strtotime("now"); 
     echo "<hr>";
     echo mktime( $hour, $minute, $second, $month, $day, $year);
    ?>
    
	</div>
    
    <!--Jerome solution ************************************************************************************ -->
<?php //if ($txt_animals != "" && $txt_count != "") {
        //$plural = ($txt_count != 1);
        //echo $plural ? "There are $txt_count $txt_animals"."s!" : "There is 1 $txt_animals!";
    //} elseif ($txt_error != "") { ?>
       
</body>
</html>