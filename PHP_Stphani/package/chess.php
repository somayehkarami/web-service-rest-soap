<?php 
$count =8; //number of rows and cols
//check request method
if ( $_SERVER['REQUEST_METHOD'] == "POST" ){
    //set number of squares
    $count =$_POST['squares'];
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chessboard</title>
    <style>
    table{
       
        border-collapse: collapse;
    }
    td{
        border:2px solid black;
        padding: 20px;
        
    }
    .bg_black{ 
      background-color: black;
    }
   </style>
   </head>
<body>
<!-- submit form to another page, we can control what happens -->
<form action="chess.php" method="POST">
		Squares : <input type="text" name="squares">
		<input type="submit">
	</form>
	<hr>
<table>
			<?php 
			// for loop for rows
			for ( $rows = 0; $rows < $count; $rows++){
				echo "<tr>";//for row
					//for loop for cols
					for ($cols = 0; $cols < $count; $cols++){ 
						/* 
						// chek with rows need to be darken, must stagger between rows and cols
						if ( ( ( $cols % 2 ) + ( $rows % 2) ) == 1){
							$class = "bg_black";
						} else {
							$class = "";
						} */
						// one line conditional, same result as conditional statement above
						$class = ( ( ( $cols % 2 ) + ( $rows % 2) ) == 1) ? "bg_black" : "";
						echo "<td class='$class'></td>";//for col
					} // end of col forloop
				echo "</tr>";
			}	//end of rows forloop ?>
		</table>
	</body>
</html>