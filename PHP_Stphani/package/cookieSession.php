<?php //Opening tag

//start session
session_start();//Before any output to browser

$_SESSION['username'] = "SomayehKararmi";

echo $_SESSION['username'] . "<br>";

//create expiration time for cookie
$expiration = time() + 3;// now + 60secons in UNIX timestamp;

//create cookie 
setcookie("name", "Somayeh",$expiration);
setcookie("city", "456",$expiration);

//access cookie data 
echo $_COOKIE['name'] . "<br />";
echo $_COOKIE['city'] . "<br />";
//==================================================================
// It deletes only the variables from session
// The session still exists.
//!!!!!session_unset();
//==================================================================
// it destroys all of the data associated with the current session.
//!!!!session_destroy();



/*closing tag*/  ?>