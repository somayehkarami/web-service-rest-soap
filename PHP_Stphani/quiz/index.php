<?php /* this is our brain */

//Loads all the requried fatfree libraries
require("vendor/autoload.php");//I need to access any file that you have

//start fatfree
$f3 = Base::instance();//this is class allawes to access all class insid fatfree

//configuration
//set the path(when call the class)
//AUTOLOAD==>keyword for fatfree
$f3->set('AUTOLOAD', "App/Controllers/,App/Models/");//tell fat free where to look for controller
$f3->set("UI","App/Views/");//tell fat free where to look for Views
$f3->set("DEBUG",3);

/**Information  */
//ROUTES - dont forget htaccess!!!!!
$f3->route("GET /","InformationController->homepage");//1.class-2.function


//$f3->route("GET /", function(){echo "HELLO";});
$f3->route("GET /about", function(){echo "Steph Moreau" ;});
$f3->route("POST /about", function(){echo "Moreau Stephanie " ;});

$f3->route("GET /question","QuestionsController->current");//class-function
$f3->route("POST /question", "AnswersController->saveAnswer");
$f3->route("GET /listing","QuestionsController->listQuetions");
//@in the router indicates a varible to be passed as $prams to the function
$f3->route("GET /answers/@qid","AnswersController->fetchAnswers");

//execute fatfree
$f3->run();

//require bcosca/fatfree

