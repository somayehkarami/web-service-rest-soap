        <!--header-->

        <?php echo $this->render('header.html',NULL,get_defined_vars(),0); ?>
        <h1 class="cover-heading">Answers</h1>
        <p><?= ($question['question']) ?></p>
        <table class="table">
          <thead class="thead-light">
            <tr>
              <th>#</th>
              <th>Answer</th>
            </tr>
          </thead>
          <tbody>
              <!--set up  repeat loop-->
              <?php foreach (($query?:[]) as $item): ?>
            <tr>
              <td><?= ($item['id']) ?></td>
              <td><?= ($item['answer']) ?></td>
            </tr>
             <?php endforeach; ?>
          </tbody>
        </table>
         <!-- footer.html-->
         <?php echo $this->render('footer.html',NULL,get_defined_vars(),0); ?>
