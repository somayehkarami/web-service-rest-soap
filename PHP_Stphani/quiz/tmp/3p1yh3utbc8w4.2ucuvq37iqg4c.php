        <!--header-->

        <?php echo $this->render('header.html',NULL,get_defined_vars(),0); ?>
        <h1 class="cover-heading">Listing</h1>
        <table class="table">
          <thead class="thead-light">
            <tr>
              <th>#</th>
              <th>Question</th>
              <th>Answers</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach (($data?:[]) as $key=>$item): ?>
            <tr>
              <td><?= ($item['id']) ?></td>
              <td><a href="answers/<?= ($item['id']) ?>"><?= ($item['question']) ?></a></td><!--answer to spicific question-->
              <td><?= ($item['answers']) ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
     <!-- footer.html-->
     <?php echo $this->render('footer.html',NULL,get_defined_vars(),0); ?>

     <!--id
    Question 
  Answer
columns of tables in listing page-->
