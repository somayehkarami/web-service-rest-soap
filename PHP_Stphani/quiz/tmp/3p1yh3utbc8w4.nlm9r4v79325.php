<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--conditional  title -->
    <title>
        <?php if ($title !=''): ?>
              <?= ($title) ?> - 
             <?php else: ?>My Homepage for
        <?php endif; ?>
    Quiz</title>
    
 
    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">
 
    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.0/examples/cover/cover.css" rel="stylesheet">
  </head>
 
  <body class="text-center">
 
    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
        
      <header class="masthead mb-auto">
        <div class="inner">
          <h3 class="masthead-brand">Cover</h3>
          <!--menu.html-->
          <?php echo $this->render('menu.html',NULL,get_defined_vars(),0); ?>
        </div>
      </header>
 
      <main role="main" class="inner cover">