<?php //Models/Question.php
//Models intract with database where we query.


class Questions extends DB\SQL\Mapper{

    function __construct(DB\SQL $db){
        //set my parent know about the db connection and table to use
        parent::__construct($db, 'questions');//name of table 'questions'!!!!!
    }

    function latestQuestion(){
        //first argument is Where, second argument are optional
        $this->load();//array(),array("LIMIT"=>1,"ORDER"=>"id DESC"));
        //select * from Question order by id desc limit 1;

        //resultes of the sql statment as an array
        return $this->query[1];//-> for object
        //[0] index zero because I know Iam only getting 1 row i return index 0
    }
    function allQuestions(){
        $this->load();
        //selsect * from questions

        return $this->query;
    }

    function getById($id){
        $this->load(array("id=?", $id));
        //select * from questions where id = $id

        return $this->query[0];

    }

    	// increament the answers integer by 1
	function increaseAnswer( $id ){
		
		$this->load( array( "id=?", $id ) );// get all the information about the question 
			// SELECT * FROM questions WHERE id = $id
		$this->answers++;// increase the count
		$this->save();//save to database
	}

}
?>