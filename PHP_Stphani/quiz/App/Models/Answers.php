<?php // Answers model
 
class Answers extends DB\SQL\Mapper{
 
  function __construct(DB\SQL $db){
    parent::__construct($db, 'answers');

    
  }
 
  function fetch_by_question($qid){
    $this->load( array( "question_id=?",$qid ) );
    
    return $this->query;
  }
 //insert new row
 
  function  createAnswer(){
     $this->completed = date("Y-m-d H:i:s");
     $this->copyFrom('POST');//knows answer has id complted like $this->id....
     $this->save();//execute


 }
 
}