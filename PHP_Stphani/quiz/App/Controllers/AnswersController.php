<?php
// AnswersController
//related to answer table
 
class AnswersController extends Controller{
 
	private $model;//access to the  database model
 
	function __construct(){
		parent::__construct();
 
		$this->model = new Answers($this->db);
	}
 
    //get the list of all answers for  spicific question 
	function fetchAnswers($f, $params){// /quiz/answer/4//get information from url=>params
		//get the answers
		$f->set('query', $this->model->fetch_by_question($params['qid']));
		//'query' is for view
	
		// get the question
		$ctrlQuestion = new Questions($this->db);
		$f->set('question', $ctrlQuestion->getById($params['qid']));
		$f->set("title", " Answers");
 
		$template = new Template;
        echo $template->render("answers.html");
	
	}
 
	function saveAnswer($f){
		$ctrlQuestion = new Questions($this->db);
		$f->set("title", " Answers");
        // $f->get('POST.answer') is almost the same as $_POST['answer']
		if ( $f->get('POST.answer') != "" )  {
			//insert answer into db
 
			$this->model->createAnswer();//save to database 
			
		// increment answers on questions table
		$ctrlQuestion->increaseAnswer( $f->get('POST.question_id') );

			$f->reroute('/answers/'. $f->get('POST.question_id'));//redrict to new page
 
		} else {
			//show error
			$f->set("error", "No answer was provided");
            //get information about 
			$ctrlQuestion = new Questions($this->db);
		    $f->set('data', $ctrlQuestion->getById( $f->get('POST.question_id')));
			$template = new Template;
			echo $template->render("currentQuestion.html");
		}
 
 
	}
}
