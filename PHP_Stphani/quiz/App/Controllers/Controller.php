<?php // Controller/Controller.php
 
class Controller{
 
  protected $db;
 
  function __construct(){
 
		//setting up db instance so we can use it
    $this->db = new DB\SQL(//connect to database
      "mysql:host=localhost;port=3306;dbname=ipd25_class", //db name
			"ipd25", //db user
			"ipd25_pw" // db password
    );
	}
}