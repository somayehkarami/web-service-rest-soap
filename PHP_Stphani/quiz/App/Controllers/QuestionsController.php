<?php
//QuestionController

class QuestionsController extends Controller{

    private $model;//how to connect to our spicific table

    //executed as soon as class is initilized
    function __construct(){
        parent::__construct();//ensure parent constructor gets executed

        //connect to the table for this controller
        $this->model = new Questions($this->db);//access to database

    }

     function current($f){//$f is the $f3 from index.php
        //global $f3;
        $f->set("title", "Current Question");
        $f->set("error", false);//we add this for error in 
        //$f3->set("title","Hello");
        //call model for database information
        $results = $this->model->latestQuestion();
        $f->set("data",$results);
         
         $view = new Template;
         echo $view->render("currentQuestion.html");

     }
     //Display all the question 
     function listQuetions($f){
        
         //call our model to fetch the quetions
         $results = $this->model->allQuestions();

         //save the query result for the view 
         $f->set("title", "List Questions");
         $f->set("data",$results);

         //show the view
         $view = new Template;
         echo $view->render("listing.html");
     }
}
