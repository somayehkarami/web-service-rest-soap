<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- PAGE settings -->
  <link rel="icon" href="https://templates.pingendo.com/assets/Pingendo_favicon.ico">
  <title>Checkout</title>
  <meta name="description" content="Wireframe design of a form by Pingendo">
  <meta name="keywords" content="Pingendo bootstrap example template wireframe form">
  <meta name="author" content="Pingendo">
  <!-- CSS dependencies -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="assets/wireframe.css">
</head>

<body class="bg-light">
  <div class="py-5" >
    <div class="container">
      <div class="row">
        <div class="text-center col-md-7 mx-auto"> <i class="fa d-block fa-bullseye fa-5x mb-4 text-info"></i>
          <h2><b>ReST Web Service</b></h2>
        </div>
      </div>
    </div>
  </div>
  <div class="">
    <div class="container">
      <div class="row">
        <div class="col-md-4 order-md-2">
          <h4 class="d-flex justify-content-between mb-3"> <span class="text-muted"><b>Cart item(s)</b></span> <span class="badge badge-secondary badge-pill">3<!-- TODO --></span> </h4>
          <ul class="list-group">
            <li class="list-group-item d-flex justify-content-between">
              <div>
                <h6 class="my-0"><b>Product name<!-- TODO --></b></h6> <small class="text-muted">Product description - will likely go over multiple lines<!-- TODO --></small>
              </div> <span class="text-muted">$12<!-- TODO --></span>
            </li>
            <li class="list-group-item d-flex justify-content-between">
              <div>
                <h6 class="my-0"><b>Second product<!-- TODO --></b></h6> <small class="text-muted">Brief description<!-- TODO --></small>
              </div> <span class="text-muted">$8<!-- TODO --></span>
            </li>
            <li class="list-group-item d-flex justify-content-between">
              <div>
                <h6 class="my-0"><b>Third item<!-- TODO --></b></h6> <small class="text-muted">Brief description<!-- TODO --></small>
              </div> <span class="text-muted">$10<!-- TODO --></span>
            </li>
            <li class="list-group-item d-flex justify-content-between"> <span>Total</span> <b>$20<!-- TODO --></b> </li>
          </ul>
        </div>
        <div class="col-md-8 order-md-1">
          <h4 class="mb-3" contenteditable="true"><b>Customer Information</b></h4>
          <form class="needs-validation" novalidate="">
            <div class="row">
              <div class="col-md-6 mb-3"> <label for="firstName">First name</label>
                <input type="text" class="form-control" id="firstName" placeholder="First Name" value=""><!-- TODO -->
              </div>
              <div class="col-md-6 mb-3"> <label for="lastName">Last name</label>
                <input type="text" class="form-control" id="lastName" placeholder="Last Name" value=""><!-- TODO -->
              </div>
            </div>
            <div class="mb-3"> <label for="email" contenteditable="true">Company&nbsp;<br></label>
              <input type="email" class="form-control" id="company" placeholder="Company Name" value=""><!-- TODO -->
            </div>
            <div class="mb-3"> <label for="email">Slogan <span class="text-muted"></span></label>
              <input type="email" class="form-control" id="catchPhrase" placeholder="Company Catch Phrase" value=""><!-- TODO -->
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5 text-muted text-center">
    <div class="container">
      <div class="row">
        <div class="col-md-12 my-4">
          <p class="mb-1">© IPD ReST Test</p>
          <p class="mb-1">Thank you for a wonderful calss and good luck.</p>
        </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>