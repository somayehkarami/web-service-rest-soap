<?php

//include composer library
require "vendor/autoload.php";

/*
-install nusoap library (using composer)==>composer require econea/nusoap*
-create our function to get stock price
-create oure web server
-generate our wsdl
*/


//create function for web service
function getStockPrice($stockName){
    //Ideal to fatch data from database
    $stocks=array("IBM"=>149.82,
                  "Apple"=>38.89,
                  "Bing"=>0.45);
    //default value
    $stockPrice = -1;//set default stock price
    
    //fetch stock price
    if(isset($stocks[$stockName])){//does array exist ?
        $stockPrice = $stocks[$stockName];
    }
    return $stockPrice;// my responce==>return
}

//get stock type
function getStockType($stockName){
    $stocks=array("IBM"=>"Techonlogy",
    "Apple"=>"Techonlogy / Telecommuniction",
    "Bing"=> "Search Engine");

    $stockType ="";
   /* if(isset($stocks[$stockName])){//does array exist ?
        $stockPrice = $stocks[$stockName];
    }*/
     $stockType=(isset($stocks[$stockName])) ? $stocks[$stockName] : "";//does array exist ?
    return $stockType;// my responce==>return


}

//initilaize our web service
//this is class inside 
$server = new soap_server();//soap server instance
$server->configureWSDL("server");//configure WSDL=== method exist in my class

//define namespace
$server->wsdl->schemaTargetNamespace ="http://localhost/Ipd25-soap/soap_stock.php";

//create the method definition for web service
//name == $stockName
//name of function-input paramter what expect array 
//initialization of web service functions
$server->register("getStockPrice",
         array("name"=> "xsd:string"),// input
         array("price"=> "xsd:decimal"),// output
);

$server->register("getStockType",
array("name"=> "xsd:string"),// input
array("stockType"=> "xsd:string"),// output     
);
//start the server 
//output it to soap server
$server->service(file_get_contents("php://input"));

?>