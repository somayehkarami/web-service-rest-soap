<?php

//base url for my web service
$base_url ="https://api.chucknorris.io/jokes/";
$api_url= $base_url . "categories/";


function callAPI($url){
    //init my fetch URL commend 
    $ch = curl_init($url);//open the door to get this URL
   
   //Prevent screen output and save to varible
   curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
   
   //execute our fetch command and save to variable
   $results = curl_exec($ch);
   
   //close curl
   curl_close($ch);
   
   //convert my json string into an object
    $data =json_decode($results);
    return $data;
   }
   
  
   $catagories= callAPI($api_url);
   print_r($catagories);
   
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Chuck Norris</title>
	<style>
		#chuckTitle, #chuckImg, #chuckJoke{ display: none;}
		#chuckImg { float: left; }
	</style>
</head>
<body>
  <h1>Random Chuck Norris Facts</h1>
  <p>Select a category to view a random fact about Chuck Norris</p>
 
    <select name="cat" onchange="showJock();" id="chuckCat">
      <option value="">-----</option>
			<!-- list categories here -->
            <?php
            foreach($catagories as $c){
                echo "<option value ='$c'>$c</option>";
            }
            ?>
    </select>
  <hr />
 
 <h3 id="chuckTitle">Chuck Norris Jokes</h3>
 <img id="chuckImg" src='URL' alt='Chuck Fact Image' />
 <div id="chuckJoke">THE JOKE</div>
 <script>
  function showJock(){
  // getting the element to manipulate and assigning it to a variable
let select = document.getElementById("chuckCat");
// passing the value of the selected element to a variable
let category = select.options[select.selectedIndex].value;
  //console.log(category);//check that categoriy is working

  //start API call
  let api_url = "<?php echo $base_url; ?>random?category=" + category;
  let request = new XMLHttpRequest();
  request.open("GET", api_url);
  request.send();
  request.onload = ()=>{
      if(request.status ==200){//success
        console.log(request.response);

       let data=JSON.parse(request.response);
       console.log(data);
      

        document.getElementById("chuckJoke").innerHTML =data.value;
        document.getElementById("chuckImg").src= data.icon_url;

        document.getElementById("chuckJoke").style.display="block";
        document.getElementById("chuckImg").style.display ="inline";
        document.getElementById("chuckTitle").style.display="block";
       
    }else{//anything but 200
        let data=JSON.parse(request.response);
        console.log("ERROR: " +data.response);

      }
  }

  }
 </script>
</body>
</html>
