<!-- ===================================================================THIS IS THE WEATHER API PHP TAG================================================================= -->
<!-- =================================================================================================================================================================== -->
<?php
//make a CURL command and return json decoded object
function callAPI($url)  {
    // init my fetch URL (curl) command
    $ch = curl_init($url);

    // prevent screen output and save to variable as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // XAMPP or MAMP issues
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    // execute our fetch command and save to variable
    $results = curl_exec($ch);

    // close curl
    curl_close($ch);

    // convert my json string into an object
    $data = json_decode($results);

    return $data;
}

$accessKey = "e91acc21c0716c8ce1c52d2318f84e80";
// base url for my web service
$base_url = "http://api.weatherstack.com/";
// url of resources being requested
$api_url = $base_url . "current?access_key=e91acc21c0716c8ce1c52d2318f84e80&query=";

$data = callAPI($api_url);
// echo $data->current->temperature;
?>

<!-- ===================================================================THIS IS THE BORED API PHP TAG=================================================================== -->
<!-- =================================================================================================================================================================== -->

<?php
//make a CURL command and return json decoded object
function callBoredAPI($url)  {
    // init my fetch URL (curl) command
    $ch = curl_init($url);

    // prevent screen output and save to variable as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // XAMPP or MAMP issues
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    // execute our fetch command and save to variable
    $results = curl_exec($ch);

    // close curl
    curl_close($ch);

    // convert my json string into an object
    $data = json_decode($results);

    return $data;
}

// base url for my web service
$bored_base_url = "http://www.boredapi.com/api/activity";
// url of resources being requested
$bored_api_url = $bored_base_url . "?type=recreational";

$data = callBoredAPI($bored_api_url);
// echo $data->current->temperature;
?>



<?php
// *************************** Pokemon STUFF **************************

// base url for my web service
$base_url_pokemon = "https://pokeapi.co/api/v2/";
// url of resources being requested
$api_url_pokemon = $base_url_pokemon . "pokemon?limit=250";

$dataPokemon = callAPI($api_url_pokemon);

$pokemon = [];
foreach($dataPokemon->results as $item) {
    //echo $item->name . "<br />";
    $pokemon[] = $item->name;
}
shuffle($pokemon);
$pokemonRandom = [];
for($p = 0; $p < 4; $p++) {
    $pokemonRandom[] = $pokemon[$p];
}
?>


<!-- ===================================================================THIS IS THE GEO API PHP TAG===================================================================== -->
<!-- =================================================================================================================================================================== -->

<?php
require_once('geoplugin.class.php');
$geoplugin = new geoPlugin();

/* 
Notes:

The default base currency is USD (see http://www.geoplugin.com/webservices:currency ).
You can change this before the call to geoPlugin::locate with eg:
$geoplugin->currency = 'EUR';

The default IP to lookup is $_SERVER['REMOTE_ADDR']
You can lookup a specific IP address, by entering the IP in the call to geoPlugin::locate
eg
$geoplugin->locate('209.85.171.100');

The default language is English 'en'
supported languages:
de (German)
en (English - default)
es (Spanish)
fr (French)
ja (Japanese)
pt-BR (Portugese, Brazil)
ru (Russian)
zh-CN (Chinese, Zn)

To change the language to e.g. Japanese, use:
$geoplugin->lang = 'ja';

*/

//locate the IP
$geoplugin->locate('REMOTE_ADDR');

// echo "Geolocation results for {$geoplugin->ip}: <br />\n".
// 	"City: {$geoplugin->city} <br />\n".
// 	"Region: {$geoplugin->region} <br />\n".
// 	"Region Code: {$geoplugin->regionCode} <br />\n".
// 	"Region Name: {$geoplugin->regionName} <br />\n".
// 	"DMA Code: {$geoplugin->dmaCode} <br />\n".
// 	"Country Name: {$geoplugin->countryName} <br />\n".
// 	"Country Code: {$geoplugin->countryCode} <br />\n".
// 	"In the EU?: {$geoplugin->inEU} <br />\n".
// 	"EU VAT Rate: {$geoplugin->euVATrate} <br />\n".
// 	"Latitude: {$geoplugin->latitude} <br />\n".
// 	"Longitude: {$geoplugin->longitude} <br />\n".
// 	"Radius of Accuracy (Miles): {$geoplugin->locationAccuracyRadius} <br />\n".
// 	"Timezone: {$geoplugin->timezone} <br />\n".
// 	"Currency Code: {$geoplugin->currencyCode} <br />\n".
// 	"Currency Symbol: {$geoplugin->currencySymbol} <br />\n".
// 	"Exchange Rate: {$geoplugin->currencyConverter} <br />\n";

/*
How to use the in-built currency converter
geoPlugin::convert accepts 3 parameters
$amount - amount to convert (required)
$float - the number of decimal places to round to (default: 2)
$symbol - whether to display the geolocated currency symbol in the output (default: true)
*/
// if ( $geoplugin->currency != $geoplugin->currencyCode ) {
// 	//our visitor is not using the same currency as the base currency
// 	echo "<p>At todays rate, US$100 will cost you " . $geoplugin->convert(100) ." </p>\n";
// }

/* Finding places nearby 
nearby($radius, $maxresults)
$radius (optional: default 10)
$maxresults (optional: default 5)
 */
// $nearby = $geoplugin->nearby();

// if ( isset($nearby[0]['geoplugin_place']) ) {

// 	echo "<pre><p>Some places you may wish to visit near " . $geoplugin->city . ": </p>\n";

// 	foreach ( $nearby as $key => $array ) {
		
// 		echo ($key + 1) .":<br />";
// 		echo "\t Place: " . $array['geoplugin_place'] . "<br />";
// 		echo "\t Region: " . $array['geoplugin_region'] . "<br />";
// 		echo "\t Latitude: " . $array['geoplugin_latitude'] . "<br />";
// 		echo "\t Longitude: " . $array['geoplugin_longitude'] . "<br />";
// 	}
// 	echo "</pre>\n";
// }
?>

<!-- ===================================================================THIS IS THE HTML=============================================================================== -->
<!-- ================================================================================================================================================================== -->


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="callAPIFunction.php">
    <title>Travel Info</title>
    <style>
        body {
            text-align: center;
        }
        h1 {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            color: gray;
        }
        h2 {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            color: gray;
        }
        p {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        }
        #container {
            margin-left: 200px;
            margin-right: 200px;
            background-color: whitesmoke;
        }
        #poke {
            display: none;
        }
        #boredSection {
            display: none;
        }
        button {
            margin-bottom: 25px;
        }
    </style>
</head>
<div id="container">
<body>
    <h1>Discover more about where you're going!</h1>
    <p>Enter a location:</p>
    <!-- Text field for location -->
    <form action="" method="POST">
        <input type="text" id="location" name="locationName">
        <button type="submit" id="btnLocation" name="submit">Get the weather</button><br />
        <button type="submit" id="btnPokemon" name="submit">What pokemon are nearby?</button>
    </form>

    <h2>Travel Info:</h2>
    <p id="temp"></p>
    <p id="poke">
        Watch out! There's a 
        <?php
            foreach($pokemonRandom as $item) {
                echo $item . ", ";
            }
        ?>
        nearby!
    </p>
    <br>
    <h2 id="boredSection">Feeling bored?</h2>
    <p id="bored"></p>

    <hr>
    <h2>Here's some info about where you currently are!</h2>
    <p>
        <?php
            echo //"Geolocation results for {$geoplugin->ip}: <br />\n".
            "City: {$geoplugin->city} <br />\n".
            "Region: {$geoplugin->region} <br />\n".
            "Region Code: {$geoplugin->regionCode} <br />\n".
            "Region Name: {$geoplugin->regionName} <br />\n".
            "Country Name: {$geoplugin->countryName} <br />\n".
            "Country Code: {$geoplugin->countryCode} <br />\n".
            "In the EU?: {$geoplugin->inEU} <br />\n".
            "Latitude: {$geoplugin->latitude} <br />\n".
            "Longitude: {$geoplugin->longitude} <br />\n".
            "Radius of Accuracy (Miles): {$geoplugin->locationAccuracyRadius} <br />\n".
            "Timezone: {$geoplugin->timezone} <br />\n".
            "Currency Code: {$geoplugin->currencyCode} <br />\n".
            "Currency Symbol: {$geoplugin->currencySymbol} <br />\n".
            "Exchange Rate: {$geoplugin->currencyConverter} <br />\n";

            if ( $geoplugin->currency != $geoplugin->currencyCode ) {
                //our visitor is not using the same currency as the base currency
                echo "<p>At todays rate, US$100 will cost you " . $geoplugin->convert(100) ." </p>\n";
            }

            $nearby = $geoplugin->nearby();

            if ( isset($nearby[0]['geoplugin_place']) ) {

                echo "<pre><p>Some places you may wish to visit near " . $geoplugin->city . ": </p>\n";

                foreach ( $nearby as $key => $array ) {
                    
                    echo ($key + 1) .":<br />";
                    echo "\t Place: " . $array['geoplugin_place'] . "<br />";
                    echo "\t Region: " . $array['geoplugin_region'] . "<br />";
                    echo "\t Latitude: " . $array['geoplugin_latitude'] . "<br />";
                    echo "\t Longitude: " . $array['geoplugin_longitude'] . "<br />";
                }
                echo "</pre>\n";
            }
        ?>
    </p>

<!-- ===================================================================THIS IS THE WEATHER SCRIPT====================================================================== -->
<!-- =================================================================================================================================================================== -->
    <script>
        document.getElementById("btnLocation").addEventListener("click", getWeather);
        function getWeather(event){
            event.preventDefault();
            //start API call
            let location = document.getElementById("location").value;
            let request = new XMLHttpRequest();
            request.open("GET", "<?php echo $api_url; ?>" + location);
            request.send();
            request.onload = () => {
                if(request.status == 200){
                    console.log( request.response);
                    let data = JSON.parse(request.response);
                    console.log(data);

                    document.getElementById("temp").innerHTML = "Temperature: " + data.current.temperature + "C";
                } else{
                    let data = JSON.parse(request.response);
                    console.log("ERROR " + data.error);
                }  
            }             
        }
    </script>

<!-- =====================================================================THIS IS THE BORED SCRIPT====================================================================== -->
<!-- =================================================================================================================================================================== -->

    <script>
        document.getElementById("btnLocation").addEventListener("click", getSuggestion);
        function getSuggestion(event){
            event.preventDefault();
            document.getElementById("boredSection").style.display = "block";
            //start API call
            let request = new XMLHttpRequest();
            request.open("GET", "<?php echo $bored_api_url; ?>");
            request.send();
            request.onload = () => {
                if(request.status == 200){
                    console.log( request.response);
                    let data = JSON.parse(request.response);
                    console.log(data);

                    document.getElementById("bored").innerHTML = "You can... " + data.activity + "!";
                } else{
                    let data = JSON.parse(request.response);
                    console.log("ERROR " + data.error);
                }  
            }             
        }
    </script>

<!-- ********************** FUNCTION FOR Pokemon Stuff ****************** -->
    <script>
        
        document.getElementById("btnPokemon").addEventListener("click", findPokemon);
        function findPokemon(event){
            event.preventDefault();
            document.getElementById("poke").style.display = "inline";            
        }
    </script>
</body>
</div>

</html>
