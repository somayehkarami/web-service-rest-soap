<?php
//make a CURL command and return json decoded object
function callAPI($url)  {
    // init my fetch URL (curl) command
    $ch = curl_init($url);

    // prevent screen output and save to variable as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // XAMPP or MAMP issues
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    // execute our fetch command and save to variable
    $results = curl_exec($ch);

    // close curl
    curl_close($ch);

    // convert my json string into an object
    $data = json_decode($results);

    return $data;
}
$accessKey = "e91acc21c0716c8ce1c52d2318f84e80";
// base url for my web service
$base_url = "http://api.weatherstack.com/";
// url of resources being requested
$api_url = $base_url . "current?access_key=e91acc21c0716c8ce1c52d2318f84e80&query=";

$data = callAPI($api_url);
// echo $data->current->temperature;
?>
<?php
//make a CURL command and return json decoded object
function callBoredAPI($url)  {
    // init my fetch URL (curl) command
    $ch = curl_init($url);

    // prevent screen output and save to variable as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // XAMPP or MAMP issues
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    // execute our fetch command and save to variable
    $results = curl_exec($ch);

    // close curl
    curl_close($ch);

    // convert my json string into an object
    $data = json_decode($results);

    return $data;
}

// base url for my web service
$bored_base_url = "http://www.boredapi.com/api/activity";
// url of resources being requested
$bored_api_url = $bored_base_url . "?type=recreational";

$data = callBoredAPI($bored_api_url);
// echo $data->current->temperature;
?>



<?php
// *************************** Pokemon STUFF **************************

// base url for my web service
$base_url_pokemon = "https://pokeapi.co/api/v2/";
// url of resources being requested
$api_url_pokemon = $base_url_pokemon . "pokemon?limit=250";

$dataPokemon = callAPI($api_url_pokemon);

$pokemon = [];
foreach($dataPokemon->results as $item) {
    //echo $item->name . "<br />";
    $pokemon[] = $item->name;
}
shuffle($pokemon);
$pokemonRandom = [];
for($p = 0; $p < 4; $p++) {
    $pokemonRandom[] = $pokemon[$p];
}
?>

<?php
require_once('geoplugin.class.php');
$geoplugin = new geoPlugin();
$geoplugin->locate('REMOTE_ADDR');
?>

<!doctype html>
<html lang="en">
  <head>
    <title>Travel Info</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        body {
            text-align: center;
         
            background-color:black;
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: fixed;
            color : white;

        }
        h1 {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            color: white;
           
        }
        h2 {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            color: white;
        }
        p {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        }
        #container {
            margin-left: 200px;
            margin-right: 200px;
            background-color: whitesmoke;
        }
        #poke {
            display: none;
        }
        #boredSection {
            display: none;
        }
        button {
            margin-bottom: 25px;
        }

        .cardStyle{
          margin-left: 220px;
        }
        </style>
  </head>
  <body>
  
  <h1 class="mt-5">Discover more about where you're going!</h1>
    <p>Enter a location:</p>
    <!-- Text field for location -->
    <form action="" method="POST">
        <input type="text" id="location" name="locationName">
        <button type="submit" id="btnLocation" name="submit">Get the weather</button><br />
        <button type="submit" id="btnPokemon" name="submit">What pokemon are nearby?</button>
    </form>

    <h2>Travel Info:</h2>
    <!-- Modal gallery -->
<section class="ml-5 mr-5 mt-5">
  <!-- Section: Images -->
  <section class="">
    <div class="row">
      <div class="col-lg-4 col-md-12 mb-4 mb-lg-0">
        <div
          class="bg-image hover-overlay ripple shadow-1-strong rounded"
          data-ripple-color="light"
        >
          <img
            src="https://mdbootstrap.com/img/screens/yt/screen-video-1.jpg"
            class="w-100"
          />
          <a href="#!" data-mdb-toggle="modal" data-mdb-target="#exampleModal1">
            <div class="mask" style="background-color: rgba(251, 251, 251, 0.2);"></div>
          </a>
        </div>
      </div>

      <div class="col-lg-4 mb-4 mb-lg-0">
        <div
          class="bg-image hover-overlay ripple shadow-1-strong rounded"
          data-ripple-color="light"
        >
          <img
            src="https://mdbootstrap.com/img/screens/yt/screen-video-2.jpg"
            class="w-100"
          />
          <a href="#!" data-mdb-toggle="modal" data-mdb-target="#exampleModal2">
            <div class="mask" style="background-color: rgba(251, 251, 251, 0.2);"></div>
          </a>
        </div>
      </div>

      <div class="col-lg-4 mb-4 mb-lg-0">
        <div
          class="bg-image hover-overlay ripple shadow-1-strong rounded"
          data-ripple-color="light"
        >
          <img
            src="https://mdbootstrap.com/img/screens/yt/screen-video-3.jpg"
            class="w-100"
          />
          <a href="#!" data-mdb-toggle="modal" data-mdb-target="#exampleModal3">
            <div class="mask" style="background-color: rgba(251, 251, 251, 0.2);"></div>
          </a>
        </div>
      </div>
    </div>
  </section>
  <!-- Section: Images -->

  <!-- Section: Modals -->
  <section class="">
    <!-- Modal 1 -->
    <div
      class="modal fade"
      id="exampleModal1"
      tabindex="-1"
      aria-labelledby="exampleModal1Label"
      aria-hidden="true"
    >
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="ratio ratio-16x9">
            <iframe
              src="img1.jpeg"
              title="YouTube video"
              allowfullscreen
            ></iframe>
          </div>

          <div class="text-center py-3">
            <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal 2 -->
    <div
      class="modal fade"
      id="exampleModal2"
      tabindex="-1"
      aria-labelledby="exampleModal2Label"
      aria-hidden="true"
    >
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="ratio ratio-16x9">
            <iframe
              src="https://www.youtube.com/embed/wTcNtgA6gHs"
              title="YouTube video"
              allowfullscreen
            ></iframe>
          </div>

          <div class="text-center py-3">
            <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal 3 -->
    <div
      class="modal fade"
      id="exampleModal3"
      tabindex="-1"
      aria-labelledby="exampleModal3Label"
      aria-hidden="true"
    >
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="ratio ratio-16x9">
            <iframe
              src="https://www.youtube.com/embed/vlDzYIIOYmM"
              title="YouTube video"
              allowfullscreen
            ></iframe>
          </div>

          <div class="text-center py-3">
            <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Section: Modals -->
</section>
<!-- Modal gallery -->
    <p id="temp"></p>
    <p id="poke">
        Watch out! There's a 
        <?php
            foreach($pokemonRandom as $item) {
                echo $item . ", ";
            }
        ?>
        nearby!
    </p>
    <br>
    <h2 id="boredSection">Feeling bored?</h2>
    <p id="bored"></p>

    <hr>
    <h2>Here's some info about where you currently are!</h2>
    <p>
    <?php
       
            echo //"Geolocation results for {$geoplugin->ip}: <br />\n".
            "City: {$geoplugin->city} <br />\n".
            "Region: {$geoplugin->region} <br />\n".
            "Region Code: {$geoplugin->regionCode} <br />\n".
            "Region Name: {$geoplugin->regionName} <br />\n".
            "Country Name: {$geoplugin->countryName} <br />\n".
            "Country Code: {$geoplugin->countryCode} <br />\n".
            "In the EU?: {$geoplugin->inEU} <br />\n".
            "Latitude: {$geoplugin->latitude} <br />\n".
            "Longitude: {$geoplugin->longitude} <br />\n".
            "Radius of Accuracy (Miles): {$geoplugin->locationAccuracyRadius} <br />\n".
            "Timezone: {$geoplugin->timezone} <br />\n".
            "Currency Code: {$geoplugin->currencyCode} <br />\n".
            "Currency Symbol: {$geoplugin->currencySymbol} <br />\n".
            "Exchange Rate: {$geoplugin->currencyConverter} <br />\n";
            

            if ( $geoplugin->currency != $geoplugin->currencyCode ) {
                //our visitor is not using the same currency as the base currency
                echo "<p>At todays rate, US$100 will cost you " . $geoplugin->convert(100) ." </p>\n";
            }

            $nearby = $geoplugin->nearby();

           /* if ( isset($nearby[0]['geoplugin_place']) ) {

                echo "<pre><p>Some places you may wish to visit near " . $geoplugin->city . ": </p>\n";

                foreach ( $nearby as $key => $array ) {
                    
                    echo ($key + 1) .":<br />";
                    echo "\t Place: " . $array['geoplugin_place'] . "<br />";
                    echo "\t Region: " . $array['geoplugin_region'] . "<br />";
                    echo "\t Latitude: " . $array['geoplugin_latitude'] . "<br />";
                    echo "\t Longitude: " . $array['geoplugin_longitude'] . "<br />";
                }
                echo "</pre>\n";
            }*/
        ?>
        
      


<div class="d-flex flex-row">
  <div class="p-2">
  <div class="card  cardStyle " style="width: 22rem;  height: 950px">
       <img class="card-img-top" src="pig.jpeg" alt="Card image cap">
       <div class="card-body">
        <p class="card-text">
        <?php
        if ( isset($nearby[0]['geoplugin_place']) ) {

            echo "<pre><p>Some places you may wish to visit near " . $geoplugin->city . ": </p>\n";

     foreach ( $nearby as $key => $array ) {
                    
    echo ($key + 1) .":<br />";
    echo "\t Place: " . $array['geoplugin_place'] . "<br />";
    echo "\t Region: " . $array['geoplugin_region'] . "<br />";
    echo "\t Latitude: " . $array['geoplugin_latitude'] . "<br />";
    echo "\t Longitude: " . $array['geoplugin_longitude'] . "<br />";
   }
    echo "</pre>\n";
    }

        ?>
        

        </p>
        

</div>
</div>
  </div>
  <div class="p-2"><!-- Gallery -->
<div class="row">
  <div class="col-lg-4 col-md-12 mb-4 mb-lg-0 ml-5">
    <img
      src="img2.jpeg"
      class="w-100 shadow-1-strong rounded mb-4"
      alt="Responsive image"
    />

    <img
      src="https://mdbootstrap.com/img/Photos/Vertical/mountain1.jpg"
      class="w-100 shadow-1-strong rounded mb-4"
      alt="Responsive image"
    />
  </div>

  <div class="col-lg-4 mb-4 mb-lg-0">
    <img
      src="https://mdbootstrap.com/img/Photos/Vertical/mountain2.jpg"
      class="w-100 shadow-1-strong rounded mb-4"
      alt="Responsive image"
    />

    <img
      src="img1.jpeg"
      class="w-100 shadow-1-strong rounded mb-4"
      alt="Responsive image"
    />
  </div>

  
</div>
<!-- Gallery -->
</div>
  
</div>
    
    <script>
        document.getElementById("btnLocation").addEventListener("click", getWeather);
        function getWeather(event){
            event.preventDefault();
            //start API call
            let location = document.getElementById("location").value;
            let request = new XMLHttpRequest();
            request.open("GET", "<?php echo $api_url; ?>" + location);
            request.send();
            request.onload = () => {
                if(request.status == 200){
                    console.log( request.response);
                    let data = JSON.parse(request.response);
                    console.log(data);

                    document.getElementById("temp").innerHTML = "Temperature: " + data.current.temperature + "C";
                } else{
                    let data = JSON.parse(request.response);
                    console.log("ERROR " + data.error);
                }  
            }             
        }
    </script>

<!-- =====================================================================THIS IS THE BORED SCRIPT====================================================================== -->
<!-- =================================================================================================================================================================== -->

    <script>
        document.getElementById("btnLocation").addEventListener("click", getSuggestion);
        function getSuggestion(event){
            event.preventDefault();
            document.getElementById("boredSection").style.display = "block";
            //start API call
            let request = new XMLHttpRequest();
            request.open("GET", "<?php echo $bored_api_url; ?>");
            request.send();
            request.onload = () => {
                if(request.status == 200){
                    console.log( request.response);
                    let data = JSON.parse(request.response);
                    console.log(data);

                    document.getElementById("bored").innerHTML = "You can... " + data.activity + "!";
                } else{
                    let data = JSON.parse(request.response);
                    console.log("ERROR " + data.error);
                }  
            }             
        }
    </script>

<!-- ********************** FUNCTION FOR Pokemon Stuff ****************** -->
    <script>
        
        document.getElementById("btnPokemon").addEventListener("click", findPokemon);
        function findPokemon(event){
            event.preventDefault();
            document.getElementById("poke").style.display = "inline";            
        }
    </script>
  
      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>


