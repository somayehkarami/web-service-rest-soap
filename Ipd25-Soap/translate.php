<?php

$base_url = "https://api.funtranslations.com/translate/";
$api_url = $base_url . "pirate";

$post_data ="text=Friday is a perfect day yaaa";


function callPostAPI($url, $fields){
    $ch = curl_init($url);//initialize curl request

    curl_setopt($ch,  CURLOPT_RETURNTRANSFER, 1); // no output to screen )

    // POST METHOD REQUIREMENTS!!
	// set HTTP request method to POST
    curl_setopt($ch,  CURLOPT_POST, true);

    // set my POST fields
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $fields);
    
    $results = curl_exec($ch);//execute curl request

    $data = json_decode($results);

    return $data;

}

 $data= callPostAPI($api_url, $post_data );
 if ( isset ($data->success) ){
	echo $data->contents->translated;
} else { 
	echo "An error has occured! <br>" . $data->error->message;
}


