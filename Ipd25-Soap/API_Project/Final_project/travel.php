<?php
//make a CURL command and return json decoded object
function callAPI($url)  {
    // init my fetch URL (curl) command
    $ch = curl_init($url);

    // prevent screen output and save to variable as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // XAMPP or MAMP issues
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    // execute our fetch command and save to variable
    $results = curl_exec($ch);

    // close curl
    curl_close($ch);

    // convert my json string into an object
    $data = json_decode($results);

    return $data;
}

$accessKey = "9678fb9c60e774357549a4da4dcf7361";
// base url for my web service
$base_url_weather = "http://api.weatherstack.com/";
// url of resources being requested
$api_url_weather = $base_url_weather . "current?access_key=9678fb9c60e774357549a4da4dcf7361&query=";

$data = callAPI($api_url_weather);
// echo $data->current->temperature;

// ***** Pokemon STUFF *****

// base url for my web service
$base_url_pokemon = "https://pokeapi.co/api/v2/";
// url of resources being requested
$api_url_pokemon = $base_url_pokemon . "pokemon?limit=250";

$dataPokemon = callAPI($api_url_pokemon);

$pokemon = [];
foreach($dataPokemon->results as $item) {
    //echo $item->name . "<br />";
    $pokemon[] = $item->name;
}
shuffle($pokemon);
$pokemonRandom = [];
for($p = 0; $p < 4; $p++) {
    $pokemonRandom[] = $pokemon[$p];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Travel Info</title>
    <style>
        body {
            text-align: center;
        }
        h1 {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            color: gray;
        }
        h2 {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            color: gray;
        }
        p {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        }
        #container {
            margin-left: 200px;
            margin-right: 200px;
            background-color: whitesmoke;
        }
        #poke {
            display: none;
        }
    </style>
</head>
<body>
<h1>Discover more about where you're going</h1>
    <p>Enter a location:</p>
    <!-- Text field for location -->
    <form action="" method="POST">
        <input type="text" id="location" name="locationName">
        <button type="submit" id="btnLocation" name="submit">Get the weather</button>
        <button type="submit" id="btnPokemon" name="submit">What pokemon are nearby?</button>
    </form>
    
</body>
</html>