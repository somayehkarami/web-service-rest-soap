<?php
//69c48924b84197b92c3ba67aab1ce09e
 $error="";
 $weather="";
if(array_key_exists('submit', $_GET)){
  //checking if input is empty
  if(!$_GET['city']){
    $error = "sorry, Your Input field is empty";
  }
  if($_GET['city']){
   $apiData = file_get_contents("https://api.openweathermap.org/data/2.5/weather?q=". $_GET['city'] ."&appid=c2225cf197940e87a686777b917be878");
    $weatherArray= json_decode($apiData, true);

    if($weatherArray['cod'] ==200){
      //c=k - 273.15
    $tempCon = $weatherArray['main']['temp'] - 273;
    $pressure ="<b> Atomosperic Pressure : </b>". $weatherArray['main']['pressure']."hPa";
    //print_r($weather_data);
    $temp="<b>".$weatherArray['name'].", " .$weatherArray['sys']['country']." :".intval($tempCon)."&deg;C</b> <br>";
    $wind = "<b> Wind Speed : </b>". $weatherArray['wind']['speed']."meter/sec ";
    $cloud = "<b>cloudness : </b>". $weatherArray['clouds']['all']."% ";
    //date_default_timezone_set('Canada/Montreal');
    date_default_timezone_set('UTC');
    $sunrise = $weatherArray['sys']['sunrise'];
    $sun = "<b>Sunrise : </b>".date(" g:i a", $sunrise);   
    $time = "<b>Current Time : </b>".date("F j,Y, g:i a");  
    //$time = "<b>Current Time : </b>".date('l jS \of F Y h:i:s A');  
    //'l jS \of F Y h:i:s A'
    $weather= $temp."<b>Weather condition :</b>" .$weatherArray['weather']['0']['description']."<br>" .$pressure ."<br>". $wind ."<br>". $cloud."<br>" .$sun."<br>".$time;
   
    //intval==> get integer value
    }else{
      $error="Not Found, Your city name is not valid ";
    }
    
  }

}


?>


<!doctype html>
<html lang="en">
  <head>
    <title>Weather App</title>
    <style>
      body{
        margin: 0px;
        padding: 0px;
        box-sizing: border-box;
        background-image:url(background.jpeg);
        color:white;
        background-repeat: no-repeat;
        background-size: cover;
        background-attachment: fixed;
        font-size:large;
      }
      
      .container{
        text-align:center;
        justify-content:center;
        align-items: center;
        width:440px;
      }
      .head{
        font-weight: 700;
        margin-top: 200px;
        color:white;
      }
      input{
        width: 250px;
      }
      
    </style>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
      <div class="container">
        <h1 class="head">Search Global Weather</h1>
        <form action="" method="GET">
          <p><label for="city">Enter your city name</label></p>  
           <p><input type="text" name="city" id="city" placeholder="City Name" ></p>
            <button type="submit" name="submit" class="btn btn-success">Submit</button>
            <div class="output mt-3">
               
                 <?php 
                
                 if($weather){
                   echo '<div class="alert alert-success" role="alert">'. $weather.'</div>';
                 }

                 if($error){
                   echo '<div class="alert alert-danger" role="alert">' .$error.'</div>';
                 }
                  ?>
                  
                </div>
            </div>
        </form>
      </div>
      

      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>