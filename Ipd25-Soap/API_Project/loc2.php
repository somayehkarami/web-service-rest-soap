<?php

//base url for my web service
$api_url ="http://api.geonames.org/";



//URL of resourse being requestes
//$api_url= $base_url . $_SERVER['REMOTE_ADDR'];

//$api_url =var_export(unserialize(file_get_contents('http://api.ipstack.com/'.$_SERVER['REMOTE_ADDR'])));


//make a CURL command and return json decoded object
function callAPI($url)  {
    // init my fetch URL (curl) command
    $ch = curl_init($url);

    // prevent screen output and save to variable as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // XAMPP or MAMP issues
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    // execute our fetch command and save to variable
    $results = curl_exec($ch);

    // close curl
    curl_close($ch);

    // quick check request - print out raw data
    //print_r($results);

    // convert my json string into an object
    $data = json_decode($results);

    return $data;
}

$fun=callAPI($api_url);
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Location</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
  <h1>Some places you may wish to visit near Montreal:</h1>
  <?php 
                
                
  echo '<div class="alert alert-success" role="alert">'.$fun.'</div>';
        
     ?>
      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>