
<?php
//make a CURL command and return json decoded object
function callAPI($url)  {
    // init my fetch URL (curl) command
    $ch = curl_init($url);

    // prevent screen output and save to variable as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // XAMPP or MAMP issues
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    // execute our fetch command and save to variable
    $results = curl_exec($ch);

    // close curl
    curl_close($ch);

    // convert my json string into an object
    $data = json_decode($results);

    return $data;
}

$accessKey = "fef6a6324f8098aac5a96fb76dd1a7ab";
// base url for my web service
$base_url = "http://api.weatherstack.com/";
// url of resources being requested
$api_url = $base_url . "current?access_key=fef6a6324f8098aac5a96fb76dd1a7ab&query=";

$data = callAPI($api_url);
// echo $data->current->temperature;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="callAPIFunction.php">
    <title>Travel Info</title>
</head>
<body>
    <h1>Discover more about where you're going!</h1>
    <p>Enter a location:</p>
    <!-- Text field for location -->
    <form action="" method="POST">
        <input type="text" id="location" name="locationName">
        <button type="submit" id="btnLocation" name="submit">Get the weather</button>
    </form>

    <hr />
    <h2>Travel Info:</h2>
    <p id="temp"></p>
    <?php 
        if(!empty($error)){
            echo "<div> $error </div>";
        }
    ?>
    <!-- Population?... geolocation API -->
    <!-- Restaurants close by?... geolocation or maps API? -->
    <!-- Map location?... Bing maps API -->
    <script>
        document.getElementById("btnLocation").addEventListener("click", getWeather);
        function getWeather(event){
            event.preventDefault();
            //start API call
            let location = document.getElementById("location").value;
            let request = new XMLHttpRequest();
            request.open("GET", "<?php echo $api_url; ?>" + location);
            request.send();
            request.onload = () => {
                if(request.status == 200){
                    console.log( request.response);
                    let data = JSON.parse(request.response);
                    console.log(data);

                    document.getElementById("temp").innerHTML = "Temperature: " + data.current.temperature + "C";
                } else{
                    let data = JSON.parse(request.response);
                    console.log("ERROR " + data.error);
                }  
            }             
        }
    </script>
</body>
</html>