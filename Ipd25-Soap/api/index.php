<?php

//set the page output content type (json format)
header("Content-Type:application/json");

//build out deck of cards
$suits = ["H" => "Hearts", "D"=> "Diamond","S"=>"Spade", "C"=> "Club"];
$numbers =["A", "2", "3", "4", "5", "6", "7", "8", "9", "0", "J", "Q", "K"];

$deck =[];
foreach($suits as $s => $s_name){
    foreach($numbers as $n){
        $deck[] = $s . $n;
        //$s=>key  $s_name => value
    }
}

//echo count($deck);
//print_r($deck);

//setup the API method - what can the API DO!
if (isset($_GET['action'])){
    //the user has an ction setup
//select multiple random cards
 
if($_GET['action'] == "card"){
    //select a random card
    $count =1;

    //select multiple random cards
    if(isset($_GET['count'])){
        //check the count is between 1 and max cards
        if(intval($_GET['count']) >=1 && intval($_GET['count']) <= count($deck)){//validation
            $count = $_GET['count'];
        }else{
            response(404,"ERROR","Count must be between 1 and number of cards in deck");
        }
    }
    $data =[];
    /*$cards = array_rand($deck, $count);
    $data =[];
    if($count == 1){
        $data[]= $deck[$cards];
    }
    print_r($cards);*/
    shuffle($deck);
    for($c=0; $c<$count; $c++){
        $data[] = $deck[$c];
    }
    //print_r($deck);
    //respond to user
    response(200,"SUCCESS", $data);


}elseif($_GET['action']== "suit"){
    //select a random suit
    $data = $suits[array_rand($suits)];//get 1 random value from suits array
   // print_r($data);
    response(200,"SUCCESS", $data);

}elseif($_GET['action'] == 'multiple'){

}else{
    response(404,"ERROR","UnKnown action");
}
}else{
    response(404,"ERROR","Missing action");
}

function response($code, $status, $data){
    //set header http code 
    header("HTTP/1.1 " .$code);//200ok or 400ok
    //set up acutul response
    //create array of information  for response
    $response =["status" => $status,
                 "data" => $data];
       
     echo json_encode($response);//convert to json string
     die();            
}
