<?php
 
// set the page output content type
header("Content-Type:application/json");
 
// build out deck of cards
$suits = ["H" => "Heart", "D" => "Diamond", "S" => "Spade", "C" => "Club"];
$numbers = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "0", "J", "Q", "K"];
 
$deck = [];
foreach ($suits as $s => $s_name){
	foreach ($numbers as $n){
		$deck[] = $s . $n;
	}
}
 
// echo count($deck);
// print_r($deck); // test out deck;
 
// SETUP THE API METHODS - WHAT CAN TEH API DO!
if ( isset( $_GET['action'] ) ){
	// the user has an action setup
	
	if ( $_GET['action'] == "card" ){
		// select a random card
		$count = 1;
 
		// select multiple random cards
		if ( isset($_GET['count']) ){
			// check the count is between 1 and max cards
			if ( intval( $_GET['count'] ) >= 1 && intval( $_GET['count'] ) <= count($deck) ){
				$count = $_GET['count'];
			} else {
				// TODO -- ERROR
			}
		}
 
		$data = [];
 
		// shuffle deck and get card(s)
		/* Option 1 - I dont like the not DRY code - hard to read
			$cards = array_rand( $deck, $count );
			if ( $count == 1 ){
				$data[] = $deck[ $cards ];
			} else {
				foreach ($cards as $c ){
					$data[] = $deck[ $c ];
				}
			}
		*/
		/* OPTION 2 */
		shuffle( $deck ); // shuffle the deck array
		for ( $c = 0; $c < $count; $c++ ){
			$data[] = $deck[ $c ];
		}
 
		print_r($data);
		// TODO -- respond to user
 
	} else if ( $_GET['action'] == "suit" ){
		// select a random suit
		$data = $suits[ array_rand( $suits ) ]; // get 1 random value from suits array
		print_r($data);
		// TODO -- respond to user
 
	} else {
		// TODO -- ERROR - unknown action
		echo "-ERROR";
	}
} else {
	// TODO -- ERROR - unknown action
	echo "-ERROR";
}
 