<?php

//include composer library
require "vendor/autoload.php";

/*
TODO
-create 6 functions 
-create our web server
-generate our wsdl
*/
/******************************celsius To Fahrenheit***************************** */
function celsiusToFahrenheit($temp){
    $fahrenheit=$temp*9/5+32;
    return $fahrenheit ;
}

/************************************Celsius to kelvin *************************** */
function celsiusTokelvin($temp)
{
    
	$kelvin=$temp+273.15;
	return $kelvin ;

}
/***************************Fahrenheit To celsius********************************* */

function fahrenheitTocelsius($temp){
    $celsius=5/9*($temp-32);
	return $celsius ;
}

/***************************Fahrenheit To Kelvin********************************* */

function fahrenheitToKelvin($temp){
    $kelvin=(($temp-32)/1.8)+273.15;
	return $kelvin ;
}

/***************************Kelvin To Celsius********************************* */
function kelvinToCelsius($temp)
{
	$celsius=$temp-273.15;
	return $celsius ;
}
/***************************Kelvin To Fahrenheit********************************* */
function kelvinToFahrenheit($temp)
{
	$fahrenheit=9/5*($temp-273.15)+32;
	return $fahrenheit ;
}


//initilaize our web service
//this is class inside 
$server = new soap_server();//soap server instance
//to menage and generate the web service
$server->configureWSDL("server");//configure WSDL=== method exist in my class

//define namespace
$server->wsdl->schemaTargetNamespace ="http://localhost/Ipd25-soap/convert.php";
//wsdl here is  a subclass



//create the method definition and data definition for web service
//name of function-input paramter what expect 
/**********************************server_celsiusToFahrenheit******************** */
$server->register("celsiusToFahrenheit",
         array("cel"=> "xsd:decimal"),// input
         array("far"=> "xsd:decimal"),// output
);

/******************************** server_Celsius to kelvin **************************** */
$server->register("celsiusTokelvin",
         array("cel"=> "xsd:decimal"),// input
         array("kel"=> "xsd:decimal"),// output
);


/***********************************server_FahrenheitToCelsius************************ */
$server->register("fahrenheitTocelsius",
         array("far"=> "xsd:decimal"),// input
         array("cel"=> "xsd:decimal"),// output
);

/***********************************server_FahrenheitToKelvin************************ */
$server->register("fahrenheitToKelvin",
         array("far"=> "xsd:decimal"),// input
         array("kel"=> "xsd:decimal"),// output
);

/***********************************server_KelvinToCelsius************************ */
$server->register("kelvinToCelsius",
         array("kel"=> "xsd:decimal"),// input
         array("cel"=> "xsd:decimal"),// output
);

/***********************************server_kelvinToFahrenheit************************ */
$server->register("kelvinToFahrenheit",
         array("kel"=> "xsd:decimal"),// input
         array("far"=> "xsd:decimal"),// output
);

//start the server 
//output it to soap server
$server->service(file_get_contents("php://input"));

?> 




